import {Observable} from 'rxjs';

import 'rxjs/add/observable/of';
import {Place} from "../../../model/place.model";

let dbSnapshot = {
  "categories": {
    "-KlZ9XJlFRp9WERLNa9j": {
      "active": false,
      "announced": false,
      "name": "Шаурмечная",
      "order": -1,
      "preview": "jpg"
    },
    "-KmH5-gipxvyFpniGWo8": {
      "active": true,
      "announced": false,
      "cityId": "-KzDWQGv8sRnH-u9XagQ",
      "cityName": "Зюзино",
      "name": "Кальянные",
      "order": 1,
      "preview": "jpg"
    },
    "-KobDLmwrWl0OzPeq0ar": {
      "active": true,
      "announced": false,
      "name": "Рестораны",
      "order": 3,
      "preview": "jpg"
    },
    "-KzP-cS1ZXerehoSG1zf": {
      "active": true,
      "announced": false,
      "name": "Саранская категория",
      "order": 4,
      "preview": "jpg"
    },
    "-KzP6zEgp9yQW7NTgbV8": {
      "active": false,
      "announced": false,
      "cityId": "",
      "cityName": "",
      "name": "123",
      "order": -1,
      "preview": "jpg"
    }
  },
  "categoryPlaces": {
    "category_1": {
      "place_id": 3
    },
    "category_2": {
      "place_id": 4,
      "place_id_2": 0,
    },
    "category_3": {
      "place_id_2": 4,
    }
  },
  "places": {
    "-KlZAaTZti7mS_Q0gq4r": {
      "active": true,
      "address": "Саранск,  Пролетарская ул., 108",
      "categories": {
        "-KmH5-gipxvyFpniGWo8": true,
        "-KobDLmwrWl0OzPeq0ar": true
      },
      "check": 120,
      "description": "Понятие «фаст кэжуал» является подобающим выбором для современных людей с  быстрым темпом жизни. Образ жизни в мегаполисе требует много сил и энергии, одновременно с постоянной нехваткой времени. Главное достоинство формата – возможность выполнения в одном заведении сразу две потребности гостя – поесть и провести время. Привлекательность для посетителей обусловлена в первую очередь тем, что еда здесь вкуснее и разнообразнее, качество продуктов выше на порядок, чем в фаст-фуде, а обслуживание быстрее, чем в ресторане.\n\nМы решили создать симбиоз грузинской и восточной кухни в формате «фаст кэжуал», отобрав лучшие и качественные позиции, подходящие по времени приготовления. Особый акцент сделан на традиционные, старинные рецепты.",
      "extraTitle": "",
      "greet": "",
      "hours": {
        "fri": {
          "closingTime": 24,
          "openingTime": 0
        },
        "mon": {
          "closingTime": 24,
          "openingTime": 12
        },
        "sat": {
          "closingTime": 0,
          "openingTime": 0
        },
        "sun": {
          "closingTime": 0,
          "openingTime": 0
        },
        "thu": {
          "closingTime": 0,
          "openingTime": 0
        },
        "tue": {
          "closingTime": 24,
          "openingTime": 0
        },
        "wed": {
          "closingTime": 24,
          "openingTime": 0
        }
      },
      "menu": "jpg",
      "name": "Шаурма+",
      "phone": "+ 7(8342)34-45-45",
      "photos": {
        "230f903d-6677-4b39-62b5-481afcbbdf59": "jpg",
        "a2fbdb9a-163b-f270-3cc2-8abbd7b02afe": "jpg",
        "edf1dbaa-3362-a058-2814-b83ee5f8cf49": "jpg"
      },
      "preview": "jpg",
      "reserveDays": 12,
      "reserveInterval": 120,
      "services": {
        "bar": true,
        "playstation": true,
        "wifi": true,
        "xbox": true
      },
      "website": ""
    },
    "-KmWY0ZuLuXlRQfF35Fv": {
      "active": true,
      "address": "Большевистская ул., 60",
      "admin": {
        "ZYulHDT4INbUZssMlaRoL7pYd5e2": true
      },
      "categories": {
        "-KmH5-gipxvyFpniGWo8": true,
        "-KobDlv84J4hnmhTLWQW": true
      },
      "check": 900,
      "description": "HookahPlace™ Саранске - филиал всемирно известной сети кальянных заведений. Мы предлагаем новый подход к культуре курения кальяна. Без сомнений, Hookah Place делают уникальным местом кальянщики - опытные мастера, которые полностью овладели мастерством приготовления кальяна, знают его секреты и историю. \n\nКальянная в Саранске уже стала местом просвещения поклонников курения кальяна. Мы постоянно проводим регулярные мастер-классы, тренинги, обучаем кальянному мастерству. При этом, HookahPlace — лучшее место, чтобы покурить кальян недорого в Саранске. Мы поддерживаем цены на доступном уровне и даем возможность гостям недорого исследовать мир кальяна и его новые вкусы. В нашей коллекции представлены все новинки кальянного рынка, лучшие кальяны и огромный ассортимент табака.",
      "extraPhotos": {
        "4ebdc963-70fd-bbf3-6deb-f44affb62747": "jpg",
        "6882909b-157f-d79d-4195-f2a4e82ec5d5": "jpg",
        "934bea8c-54c6-59ef-c355-ce5bac958c59": "jpg",
        "9df809c4-3cce-547a-233d-bfe19cb054cc": "jpg"
      },
      "extraTitle": "Кальяны и табаки",
      "greet": "Спасибо за заказ! Вам предоставляется скидка на кальян в размере 20%.",
      "hours": {
        "fri": {
          "closingTime": 4,
          "openingTime": 14
        },
        "mon": {
          "closingTime": 0,
          "openingTime": 24
        },
        "sat": {
          "closingTime": 4,
          "openingTime": 14
        },
        "sun": {
          "closingTime": 3,
          "openingTime": 12
        },
        "thu": {
          "closingTime": 2,
          "openingTime": 14
        },
        "tue": {
          "closingTime": 2,
          "openingTime": 14
        },
        "wed": {
          "closingTime": 2,
          "openingTime": 14
        }
      },
      "menu": "",
      "name": "Hookah Place",
      "phone": "+ 7(8342)34-45-45",
      "photos": {
        "5b151ad6-59ca-a88c-17fb-6fafe8bc0729": "jpg",
        "96a3b3c2-951d-eb33-5942-514aeb8deb10": "jpg",
        "b22d8512-8521-cc22-2390-e41bb7f63a51": "jpg",
        "e63a2e03-29d8-e906-4241-f02354038c9d": "jpg"
      },
      "preview": "jpg",
      "reserveDays": 6,
      "reserveInterval": 30,
      "services": {
        "bar": true,
        "boardGame": true,
        "businessLunch": true,
        "cuisine": true,
        "deal": true,
        "hookah": true,
        "liveSport": true,
        "playstation": true,
        "showtime": true,
        "standUp": true,
        "vip": true,
        "wifi": true,
        "xbox": true
      },
      "website": "https://saransk.hookahplace.ru/Saransk"
    },
    "-KogbwtMGLxXp4sGwJbr": {
      "active": true,
      "address": "ул. Большевистская 60",
      "admin": {
        "8h5cDXOnKRTsYWmNKUmtjSFltde2": true,
        "ZYulHDT4INbUZssMlaRoL7pYd5e2": true,
        "feP4llXXReScUu70DJ8WdkOyzzt1": true,
        "qCxJn5s3QRMHtpzlRrEwjBYJ8gP2": true
      },
      "categories": {
        "-KobDLmwrWl0OzPeq0ar": true
      },
      "check": 700,
      "description": "Гриль-бар BIG PIG - мясной ресторан с открытой кухней.\n\nМожно бесконечно долго смотреть на три вещи:\nна горящий огонь,\nкак жарится мясо,\nи на людей, которые его готовят.",
      "extraPhotos": {
        "5e4e8bc5-63a2-64a7-d8ca-dd9a0a5459d4": "jpg",
        "5f2caa5f-27dd-fe58-0aa2-2d575dd78f4f": "jpg",
        "7ea73a32-3f04-558c-02f0-38a1c2c3a76f": "jpg",
        "9e608e93-40ff-c203-4b01-352e60266af8": "jpg",
        "dbda07e7-d453-8662-ad36-46d252d10fdb": "jpg"
      },
      "extraTitle": "Кухня",
      "greet": "Ваша бронь успешно добавлена. Спасибо!",
      "hours": {
        "fri": {
          "closingTime": 3,
          "openingTime": 10
        },
        "mon": {
          "closingTime": 2,
          "openingTime": 10
        },
        "sat": {
          "closingTime": 24,
          "openingTime": 10
        },
        "sun": {
          "closingTime": 4,
          "openingTime": 12
        },
        "thu": {
          "closingTime": 24,
          "openingTime": 10
        },
        "tue": {
          "closingTime": 24,
          "openingTime": 10
        },
        "wed": {
          "closingTime": 24,
          "openingTime": 10
        }
      },
      "menu": "pdf",
      "name": "Big Pig",
      "phone": "+7 (8342) 37-36-00",
      "photos": {
        "52fa6266-42ec-6e6b-e43a-9fa9e523e3ba": "jpg",
        "58a828af-6b81-8414-c96f-f9441fe0e65e": "jpg",
        "94333746-f03e-16c2-f384-3e698b30c31b": "jpg",
        "eb1b3f7b-6313-320a-0a50-88dd4c6d15ed": "jpg",
        "eb99b26a-5f30-cc68-82fd-343832c6ca5e": "jpg",
        "ff12ae76-6900-e9f4-4f93-c8c92d5aa055": "jpg"
      },
      "preview": "jpg",
      "reserveDays": 14,
      "reserveInterval": 30,
      "reservesCount": 8,
      "services": {
        "bar": true,
        "cEuropean": true,
        "cuisine": true,
        "deal": true,
        "ownAlco": true
      },
      "website": "http://thebigpig.ru/"
    },
    "-KogehJ0-VMDv6_ZpLVC": {
      "active": true,
      "address": "Васенко, 9, 1й этаж",
      "admin": {
        "ZYulHDT4INbUZssMlaRoL7pYd5e2": true,
        "rZUWOpe1PlV9ASMW3gp9om78HRu2": true
      },
      "categories": {
        "-KmH5-gipxvyFpniGWo8": true,
        "-KobDlv84J4hnmhTLWQW": true
      },
      "check": 800,
      "description": "Мы находимся в центре города. У нас вы сможете хорошо провести время и расслабиться.",
      "extraPhotos": {
        "e99abf21-9b2d-1645-05a5-ddb63efb1f43": "jpg",
        "f46dafa4-f3f5-8321-e917-8cd14f09ff0a": "jpg",
        "f5de626d-074a-c36b-f8de-5b23d372d894": "jpg"
      },
      "extraTitle": "Галлерея",
      "greet": "Привет",
      "hours": {
        "fri": {
          "closingTime": 3,
          "openingTime": 12
        },
        "mon": {
          "closingTime": 1,
          "openingTime": 12
        },
        "sat": {
          "closingTime": 3,
          "openingTime": 12
        },
        "sun": {
          "closingTime": 1,
          "openingTime": 12
        },
        "thu": {
          "closingTime": 1,
          "openingTime": 12
        },
        "tue": {
          "closingTime": 1,
          "openingTime": 12
        },
        "wed": {
          "closingTime": 1,
          "openingTime": 12
        }
      },
      "menu": "pdf",
      "name": "Summer Jam",
      "phone": "+7 (927) 276-83-07",
      "photos": {
        "412b1e2f-572b-ff55-b759-d15cd2e46c37": "jpg",
        "56e378eb-81dd-98ad-4538-27fc4f3ad9b3": "jpg",
        "73b884c7-fb28-616d-336e-45b8207e523d": "jpg",
        "bdbb1c42-e24d-2244-fd59-d7b0043a639e": "jpg",
        "dc8b94c7-0ebd-fa90-1077-27c9627893a1": "jpg"
      },
      "preview": "jpg",
      "reserveDays": 12,
      "reserveInterval": 30,
      "reservesCount": 4,
      "services": {
        "bar": true,
        "boardGame": true,
        "businessLunch": true,
        "cuisine": true,
        "deal": true,
        "hookah": true,
        "liveMusic": true,
        "liveSport": true,
        "playstation": true,
        "vip": true,
        "wifi": true
      },
      "website": "http://summerjam.ru/"
    },
    "-Kp6KpqQ5LDm0dc9ZyZc": {
      "active": true,
      "address": "Косой переулок, д. 12",
      "categories": {
        "-KlZ9XJlFRp9WERLNa9j": true
      },
      "check": 999,
      "description": "Таки хорошее местечко для проведения досуга. И бла-бла-бла.",
      "extraPhotos": {
        "0bca5b94-c2d4-4aec-e608-aac36e6f71cf": "jpg",
        "11496559-41ea-7274-f8e2-ac804605bcf6": "jpg",
        "6df02118-2412-869c-0967-826479440f87": "jpg",
        "7ab33e54-71e9-cde7-467a-4825be5ae287": "jpg",
        "9d3c5bae-bee5-026f-efc8-5003fa414fe1": "jpg",
        "be38d684-015c-a28d-9a93-5dd48821524d": "jpg"
      },
      "extraTitle": "Доп. картинки",
      "greet": "Рас-рас-рас.",
      "hours": {
        "fri": {
          "closingTime": 24,
          "openingTime": 8
        },
        "mon": {
          "closingTime": 22,
          "openingTime": 8
        },
        "sat": {
          "closingTime": 24,
          "openingTime": 12
        },
        "sun": {
          "closingTime": 24,
          "openingTime": 11
        },
        "thu": {
          "closingTime": 22,
          "openingTime": 8
        },
        "tue": {
          "closingTime": 22,
          "openingTime": 8
        },
        "wed": {
          "closingTime": 22,
          "openingTime": 8
        }
      },
      "menu": "pdf",
      "name": "Харчевня",
      "phone": "+7-937-677-77-07",
      "photos": {
        "0d273ca8-3906-1285-d63a-905652417a6b": "jpg",
        "62ac575f-d545-0daa-dc24-7964a52f5e36": "jpg",
        "b8ed5f3d-499c-224b-1460-1bda00bbd9dd": "jpg"
      },
      "preview": "jpg",
      "reserveDays": 14,
      "reserveInterval": 60,
      "services": {
        "bar": true,
        "boardGame": true,
        "cuisine": true,
        "dj": true,
        "hookah": true,
        "liveSport": true,
        "ownAlco": true,
        "playstation": true,
        "vip": true,
        "wifi": true,
        "xbox": true
      },
      "website": "somesite.ru"
    }
  },
};


export class ApiMockService {

  public static placesListObject() {
    return Object({
      "place_id": {
        "active": true,
        "address": "place_addr",
        "categories": {
          "-KmH5-gipxvyFpniGWo8": true,
          "-KobDLmwrWl0OzPeq0ar": true
        },
        "check": 120,
        "description": "place_description",
        "extraTitle": "",
        "greet": "",
        "hours": {
          "fri": {
            "closingTime": 24,
            "openingTime": 0
          },
          "mon": {
            "closingTime": 24,
            "openingTime": 12
          },
          "sat": {
            "closingTime": 0,
            "openingTime": 0
          },
          "sun": {
            "closingTime": 0,
            "openingTime": 0
          },
          "thu": {
            "closingTime": 0,
            "openingTime": 0
          },
          "tue": {
            "closingTime": 24,
            "openingTime": 0
          },
          "wed": {
            "closingTime": 24,
            "openingTime": 0
          }
        },
        "menu": "jpg",
        "name": "Шаурма+",
        "phone": "+ 7(8342)34-45-45",
        "photos": {
          "230f903d-6677-4b39-62b5-481afcbbdf59": "jpg",
          "a2fbdb9a-163b-f270-3cc2-8abbd7b02afe": "jpg",
          "edf1dbaa-3362-a058-2814-b83ee5f8cf49": "jpg"
        },
        "preview": "jpg",
        "reserveDays": 12,
        "reserveInterval": 120,
        "services": {
          "bar": true,
          "playstation": true,
          "wifi": true,
          "xbox": true
        },
        "website": ""
      },
      "-KmWY0ZuLuXlRQfF35Fv": {
        "active": true,
        "address": "Большевистская ул., 60",
        "admin": {
          "ZYulHDT4INbUZssMlaRoL7pYd5e2": true
        },
        "categories": {
          "-KmH5-gipxvyFpniGWo8": true,
          "-KobDlv84J4hnmhTLWQW": true
        },
        "check": 900,
        "description": "HookahPlace™ Саранске - филиал всемирно известной сети кальянных заведений. Мы предлагаем новый подход к культуре курения кальяна. Без сомнений, Hookah Place делают уникальным местом кальянщики - опытные мастера, которые полностью овладели мастерством приготовления кальяна, знают его секреты и историю. \n\nКальянная в Саранске уже стала местом просвещения поклонников курения кальяна. Мы постоянно проводим регулярные мастер-классы, тренинги, обучаем кальянному мастерству. При этом, HookahPlace — лучшее место, чтобы покурить кальян недорого в Саранске. Мы поддерживаем цены на доступном уровне и даем возможность гостям недорого исследовать мир кальяна и его новые вкусы. В нашей коллекции представлены все новинки кальянного рынка, лучшие кальяны и огромный ассортимент табака.",
        "extraPhotos": {
          "4ebdc963-70fd-bbf3-6deb-f44affb62747": "jpg",
          "6882909b-157f-d79d-4195-f2a4e82ec5d5": "jpg",
          "934bea8c-54c6-59ef-c355-ce5bac958c59": "jpg",
          "9df809c4-3cce-547a-233d-bfe19cb054cc": "jpg"
        },
        "extraTitle": "Кальяны и табаки",
        "greet": "Спасибо за заказ! Вам предоставляется скидка на кальян в размере 20%.",
        "hours": {
          "fri": {
            "closingTime": 4,
            "openingTime": 14
          },
          "mon": {
            "closingTime": 0,
            "openingTime": 24
          },
          "sat": {
            "closingTime": 4,
            "openingTime": 14
          },
          "sun": {
            "closingTime": 3,
            "openingTime": 12
          },
          "thu": {
            "closingTime": 2,
            "openingTime": 14
          },
          "tue": {
            "closingTime": 2,
            "openingTime": 14
          },
          "wed": {
            "closingTime": 2,
            "openingTime": 14
          }
        },
        "menu": "",
        "name": "Hookah Place",
        "phone": "+ 7(8342)34-45-45",
        "photos": {
          "5b151ad6-59ca-a88c-17fb-6fafe8bc0729": "jpg",
          "96a3b3c2-951d-eb33-5942-514aeb8deb10": "jpg",
          "b22d8512-8521-cc22-2390-e41bb7f63a51": "jpg",
          "e63a2e03-29d8-e906-4241-f02354038c9d": "jpg"
        },
        "preview": "jpg",
        "reserveDays": 6,
        "reserveInterval": 30,
        "services": {
          "bar": true,
          "boardGame": true,
          "businessLunch": true,
          "cuisine": true,
          "deal": true,
          "hookah": true,
          "liveSport": true,
          "playstation": true,
          "showtime": true,
          "standUp": true,
          "vip": true,
          "wifi": true,
          "xbox": true
        },
        "website": "https://saransk.hookahplace.ru/Saransk"
      },
      "-KogbwtMGLxXp4sGwJbr": {
        "active": true,
        "address": "ул. Большевистская 60",
        "admin": {
          "8h5cDXOnKRTsYWmNKUmtjSFltde2": true,
          "ZYulHDT4INbUZssMlaRoL7pYd5e2": true,
          "feP4llXXReScUu70DJ8WdkOyzzt1": true,
          "qCxJn5s3QRMHtpzlRrEwjBYJ8gP2": true
        },
        "categories": {
          "-KobDLmwrWl0OzPeq0ar": true
        },
        "check": 700,
        "description": "Гриль-бар BIG PIG - мясной ресторан с открытой кухней.\n\nМожно бесконечно долго смотреть на три вещи:\nна горящий огонь,\nкак жарится мясо,\nи на людей, которые его готовят.",
        "extraPhotos": {
          "5e4e8bc5-63a2-64a7-d8ca-dd9a0a5459d4": "jpg",
          "5f2caa5f-27dd-fe58-0aa2-2d575dd78f4f": "jpg",
          "7ea73a32-3f04-558c-02f0-38a1c2c3a76f": "jpg",
          "9e608e93-40ff-c203-4b01-352e60266af8": "jpg",
          "dbda07e7-d453-8662-ad36-46d252d10fdb": "jpg"
        },
        "extraTitle": "Кухня",
        "greet": "Ваша бронь успешно добавлена. Спасибо!",
        "hours": {
          "fri": {
            "closingTime": 3,
            "openingTime": 10
          },
          "mon": {
            "closingTime": 2,
            "openingTime": 10
          },
          "sat": {
            "closingTime": 24,
            "openingTime": 10
          },
          "sun": {
            "closingTime": 4,
            "openingTime": 12
          },
          "thu": {
            "closingTime": 24,
            "openingTime": 10
          },
          "tue": {
            "closingTime": 24,
            "openingTime": 10
          },
          "wed": {
            "closingTime": 24,
            "openingTime": 10
          }
        },
        "menu": "pdf",
        "name": "Big Pig",
        "phone": "+7 (8342) 37-36-00",
        "photos": {
          "52fa6266-42ec-6e6b-e43a-9fa9e523e3ba": "jpg",
          "58a828af-6b81-8414-c96f-f9441fe0e65e": "jpg",
          "94333746-f03e-16c2-f384-3e698b30c31b": "jpg",
          "eb1b3f7b-6313-320a-0a50-88dd4c6d15ed": "jpg",
          "eb99b26a-5f30-cc68-82fd-343832c6ca5e": "jpg",
          "ff12ae76-6900-e9f4-4f93-c8c92d5aa055": "jpg"
        },
        "preview": "jpg",
        "reserveDays": 14,
        "reserveInterval": 30,
        "reservesCount": 8,
        "services": {
          "bar": true,
          "cEuropean": true,
          "cuisine": true,
          "deal": true,
          "ownAlco": true
        },
        "website": "http://thebigpig.ru/"
      },
      "-KogehJ0-VMDv6_ZpLVC": {
        "active": true,
        "address": "Васенко, 9, 1й этаж",
        "admin": {
          "ZYulHDT4INbUZssMlaRoL7pYd5e2": true,
          "rZUWOpe1PlV9ASMW3gp9om78HRu2": true
        },
        "categories": {
          "-KmH5-gipxvyFpniGWo8": true,
          "-KobDlv84J4hnmhTLWQW": true
        },
        "check": 800,
        "description": "Мы находимся в центре города. У нас вы сможете хорошо провести время и расслабиться.",
        "extraPhotos": {
          "e99abf21-9b2d-1645-05a5-ddb63efb1f43": "jpg",
          "f46dafa4-f3f5-8321-e917-8cd14f09ff0a": "jpg",
          "f5de626d-074a-c36b-f8de-5b23d372d894": "jpg"
        },
        "extraTitle": "Галлерея",
        "greet": "Привет",
        "hours": {
          "fri": {
            "closingTime": 3,
            "openingTime": 12
          },
          "mon": {
            "closingTime": 1,
            "openingTime": 12
          },
          "sat": {
            "closingTime": 3,
            "openingTime": 12
          },
          "sun": {
            "closingTime": 1,
            "openingTime": 12
          },
          "thu": {
            "closingTime": 1,
            "openingTime": 12
          },
          "tue": {
            "closingTime": 1,
            "openingTime": 12
          },
          "wed": {
            "closingTime": 1,
            "openingTime": 12
          }
        },
        "menu": "pdf",
        "name": "Summer Jam",
        "phone": "+7 (927) 276-83-07",
        "photos": {
          "412b1e2f-572b-ff55-b759-d15cd2e46c37": "jpg",
          "56e378eb-81dd-98ad-4538-27fc4f3ad9b3": "jpg",
          "73b884c7-fb28-616d-336e-45b8207e523d": "jpg",
          "bdbb1c42-e24d-2244-fd59-d7b0043a639e": "jpg",
          "dc8b94c7-0ebd-fa90-1077-27c9627893a1": "jpg"
        },
        "preview": "jpg",
        "reserveDays": 12,
        "reserveInterval": 30,
        "reservesCount": 4,
        "services": {
          "bar": true,
          "boardGame": true,
          "businessLunch": true,
          "cuisine": true,
          "deal": true,
          "hookah": true,
          "liveMusic": true,
          "liveSport": true,
          "playstation": true,
          "vip": true,
          "wifi": true
        },
        "website": "http://summerjam.ru/"
      },
      "-Kp6KpqQ5LDm0dc9ZyZc": {
        "active": true,
        "address": "Косой переулок, д. 12",
        "categories": {
          "-KlZ9XJlFRp9WERLNa9j": true
        },
        "check": 999,
        "description": "Таки хорошее местечко для проведения досуга. И бла-бла-бла.",
        "extraPhotos": {
          "0bca5b94-c2d4-4aec-e608-aac36e6f71cf": "jpg",
          "11496559-41ea-7274-f8e2-ac804605bcf6": "jpg",
          "6df02118-2412-869c-0967-826479440f87": "jpg",
          "7ab33e54-71e9-cde7-467a-4825be5ae287": "jpg",
          "9d3c5bae-bee5-026f-efc8-5003fa414fe1": "jpg",
          "be38d684-015c-a28d-9a93-5dd48821524d": "jpg"
        },
        "extraTitle": "Доп. картинки",
        "greet": "Рас-рас-рас.",
        "hours": {
          "fri": {
            "closingTime": 24,
            "openingTime": 8
          },
          "mon": {
            "closingTime": 22,
            "openingTime": 8
          },
          "sat": {
            "closingTime": 24,
            "openingTime": 12
          },
          "sun": {
            "closingTime": 24,
            "openingTime": 11
          },
          "thu": {
            "closingTime": 22,
            "openingTime": 8
          },
          "tue": {
            "closingTime": 22,
            "openingTime": 8
          },
          "wed": {
            "closingTime": 22,
            "openingTime": 8
          }
        },
        "menu": "pdf",
        "name": "Харчевня",
        "phone": "+7-937-677-77-07",
        "photos": {
          "0d273ca8-3906-1285-d63a-905652417a6b": "jpg",
          "62ac575f-d545-0daa-dc24-7964a52f5e36": "jpg",
          "b8ed5f3d-499c-224b-1460-1bda00bbd9dd": "jpg"
        },
        "preview": "jpg",
        "reserveDays": 14,
        "reserveInterval": 60,
        "services": {
          "bar": true,
          "boardGame": true,
          "cuisine": true,
          "dj": true,
          "hookah": true,
          "liveSport": true,
          "ownAlco": true,
          "playstation": true,
          "vip": true,
          "wifi": true,
          "xbox": true
        },
        "website": "somesite.ru"
      }
    });
  }

  public static placeObject() {
    return Object({
      "place_id": {
        "active": true,
        "address": "place_addr",
        "categories": {
          "-KmH5-gipxvyFpniGWo8": true,
          "-KobDLmwrWl0OzPeq0ar": true
        },
        "check": 120,
        "description": "place_description",
        "extraTitle": "",
        "greet": "",
        "hours": {
          "fri": {
            "closingTime": 24,
            "openingTime": 0
          },
          "mon": {
            "closingTime": 24,
            "openingTime": 12
          },
          "sat": {
            "closingTime": 0,
            "openingTime": 0
          },
          "sun": {
            "closingTime": 0,
            "openingTime": 0
          },
          "thu": {
            "closingTime": 0,
            "openingTime": 0
          },
          "tue": {
            "closingTime": 24,
            "openingTime": 0
          },
          "wed": {
            "closingTime": 24,
            "openingTime": 0
          }
        },
        "menu": "jpg",
        "name": "Шаурма+",
        "phone": "+ 7(8342)34-45-45",
        "photos": {
          "230f903d-6677-4b39-62b5-481afcbbdf59": "jpg",
          "a2fbdb9a-163b-f270-3cc2-8abbd7b02afe": "jpg",
          "edf1dbaa-3362-a058-2814-b83ee5f8cf49": "jpg"
        },
        "preview": "jpg",
        "reserveDays": 12,
        "reserveInterval": 120,
        "services": {
          "bar": true,
          "playstation": true,
          "wifi": true,
          "xbox": true
        },
        "website": ""
      }
    })
  }

  public static place(): Place {
    let place_id = Object.keys(ApiMockService.placeObject())[0];
    let place: Place = new Place(ApiMockService.placeObject()[place_id]);
    place.setId(place_id);
    return place;
  }

  public static categoryListObject() {
    return Object({
      "category_id_1": {
        "active": false,
        "announced": false,
        "name": "Шаурмечная",
        "order": -1,
        "preview": "jpg"
      },
      "category_id_2": {
        "active": true,
        "announced": false,
        "cityId": "city_id",
        "cityName": "city_name",
        "name": "Кальянные",
        "order": 1,
        "preview": "jpg"
      },
      "category_id_3": {
        "active": true,
        "announced": false,
        "cityId": "city_id_2",
        "cityName": "city_name_2",
        "name": "Рестораны",
        "order": 3,
        "preview": "jpg"
      },
    })
  }

  public static categoryPlacesObject() {
    return Object({
      "category_1": {
        "place_id": 3
      },
      "category_2": {
        "place_id": 4,
        "place_id_2": 0,
      },
      "category_3": {
        "place_id_2": 4,
      }
    });
  }

  public static citiesListObject() {
    return Object({
      cityid_1: {
        name: 'cityname_1'
      },
      cityid_2: {
        name: 'cityname_2'
      }
    });
  }

  public static cityCategoryObject() {
    return Object({
      city_1: {
        category_1: 1,
        category_2: 0,
        category_3: 0,
      },
      city_2: {
        category_4: 0,
        category_5: -1,
        category_6: 1,
      }
    });
  }
}
