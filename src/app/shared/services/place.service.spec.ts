import {TestBed, inject} from '@angular/core/testing';

import {Observable} from 'rxjs';

import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

import {PlaceService} from './place.service';
import {DbRepository} from "../../model/db.repository";

import {ApiMockService} from './tests/api-mock.service';
import {Place} from "../../model/place.model";

describe('PlaceService', () => {

  let subscribeSpy = jasmine.createSpy('subscribeData').and.callFake((path: string) => {
    switch (path) {
      case '/places':
        return Observable.of(ApiMockService.placesListObject());
      case '/places/place_id':
        return Observable.of(ApiMockService.placeObject());
      case '/categoryPlaces':
        return Observable.of(ApiMockService.categoryPlacesObject());
      case '/cityCategory':
        return Observable.of(ApiMockService.cityCategoryObject());
      default:
        return Observable.throw('Invalid path!');
    }
  });

  let uploadDbEntrySpy = jasmine.createSpy('uploadDbEntry');
  let setDbEntrySpy = jasmine.createSpy('setDbEntry');
  let removeDbEntrySpy = jasmine.createSpy('removeDbEntry');

  let deleteFileSpy = jasmine.createSpy('deleteFile').and.callFake((path: string) => {
    return new Promise((resolve, reject) => {
      resolve();
    });
  });
  let uploadFileSpy = jasmine.createSpy('uploadFile').and.callFake((path: string, file: File) => {
    return new Promise((resolve, reject) => {
      resolve();
    });
  });

  let dbStub = {
    subscribeData: subscribeSpy,
    uploadDbEntry: uploadDbEntrySpy,
    setDbEntry: setDbEntrySpy,
    removeDbEntry: removeDbEntrySpy,

    deleteFile: deleteFileSpy,
    uploadFile: uploadFileSpy,
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: DbRepository, useValue: dbStub},
        PlaceService]
    });
    uploadDbEntrySpy.calls.reset();
    setDbEntrySpy.calls.reset();
    removeDbEntrySpy.calls.reset();

    uploadFileSpy.calls.reset();
    deleteFileSpy.calls.reset();

  });

  it('should be created', inject([PlaceService], (service: PlaceService) => {
    expect(service).toBeTruthy();
  }));

  it('should be obtained a places list', inject([PlaceService], (service: PlaceService) => {
    service.getPlaceList()
      .subscribe((response: Place[]) => {
        expect(subscribeSpy).toHaveBeenCalled();
        expect(response).toEqual(jasmine.any(Array));

        response.map((place: Place) => {
          expect(place !== undefined).toBe(true)
        });
      });
  }));

  it('should be obtained a place', inject([PlaceService], (service: PlaceService) => {
    service.getPlaceById('place_id')
      .subscribe((response: Place) => {
        expect(subscribeSpy).toHaveBeenCalled();
        expect(response).toEqual(jasmine.any(Place));
        expect(response.getId() == 'place_id').toBe(true);
      })
  }));

  it('should be removed an image', inject([PlaceService], (service: PlaceService) => {

    service.removeMenuFile(ApiMockService.place(), () => {
    })
      .then((param) => {
        expect(deleteFileSpy).toHaveBeenCalledWith('/files/places/place_id/menu.jpg');
        expect(removeDbEntrySpy).toHaveBeenCalledWith('/places/place_id/menu');
      });
  }));

  it('should be added an image', inject([PlaceService], (service: PlaceService) => {
    let file = new File([], 'filename.jpg');
    service.addMenuFile(ApiMockService.place(), file)
      .then((param) => {
        expect(uploadFileSpy).toHaveBeenCalledWith(
          '/files/places/place_id/menu.jpg',
          file
        );
        expect(setDbEntrySpy).toHaveBeenCalledWith('/places/place_id/menu', 'jpg');
      });
  }));

  it('should be added to categoryPlaces', inject([PlaceService], (service: PlaceService) => {
    service.addPlaceToCategoryPlaces(ApiMockService.place(), {
      category_id: 10
    });
    expect(setDbEntrySpy).toHaveBeenCalledWith('/categoryPlaces/category_id/place_id', 10)

  }));

  it('should be obtained the category place list', inject([PlaceService], (service: PlaceService) => {
    service.getOrderedCategoryList(ApiMockService.place())
      .subscribe((param) => {
        expect(param).toEqual(jasmine.any(Object));
        expect(Object.keys(param).length).toEqual(3);

        Object.keys(param).map((key) => {
          expect(['category_1', 'category_2', 'category_3']).toContain(key);
          expect('category_4').not.toEqual(key)
        });
      })
  }));

  it('should be removed from categoryPlaces', inject([PlaceService], (service: PlaceService) => {
    service.removePlaceFromCategoryPlaces('place_id');
    expect(removeDbEntrySpy).toHaveBeenCalledWith('/categoryPlaces/category_1/place_id');
    expect(removeDbEntrySpy).toHaveBeenCalledWith('/categoryPlaces/category_2/place_id');
  }));
});
