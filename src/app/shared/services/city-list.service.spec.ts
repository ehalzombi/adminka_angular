import {TestBed, inject} from '@angular/core/testing';

import {Observable} from 'rxjs';

import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

import {CityListService} from './city-list.service';
import {DbRepository} from "../../model/db.repository";
import {City} from '../../model/city.model';


import {ApiMockService} from './tests/api-mock.service';

describe('CityListService', () => {

  let subscribeSpy = jasmine.createSpy('subscribeData').and.callFake((path: string) => {
    switch (path) {
      case '/cities':
        return Observable.of(ApiMockService.citiesListObject());
      case '/categories':
        return Observable.of(ApiMockService.categoryListObject());
      case '/cityCategory':
        return Observable.of(ApiMockService.cityCategoryObject());
      default:
        return Observable.throw('Invalid path!');
    }
  });

  let setDbEntrySpy = jasmine.createSpy('setDbEntry');

  let uploadDbEntrySpy = jasmine.createSpy('uploadDbEntrySpy').and.callFake(
    (fname: string) => Promise.resolve());

  let removeDbEntrySpy = jasmine.createSpy('removeDbEntrySpy').and.callFake(
    (fname: string) => Promise.resolve());

  let deleteFileSpy = jasmine.createSpy('deleteFile').and.callFake(
    (path: string) => Promise.resolve());

  let uploadFileSpy = jasmine.createSpy('uploadFile').and.callFake((path: string, file: File) => {
    return new Promise((resolve, reject) => {
      resolve();
    });
  });

  let dbStub = {
    subscribeData: subscribeSpy,
    uploadDbEntry: uploadDbEntrySpy,
    setDbEntry: setDbEntrySpy,
    removeDbEntry: removeDbEntrySpy,

    deleteFile: deleteFileSpy,
    uploadFile: uploadFileSpy,
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: DbRepository, useValue: dbStub},
        CityListService]
    });
    uploadDbEntrySpy.calls.reset();
    setDbEntrySpy.calls.reset();
    removeDbEntrySpy.calls.reset();

    uploadFileSpy.calls.reset();
    deleteFileSpy.calls.reset();

  });

  it('should be created', inject([CityListService], (service: CityListService) => {
    expect(service).toBeTruthy();
  }));

  it('should be obtainet a city list', inject([CityListService], (service: CityListService) => {
    service.getCityObjects()
      .subscribe((param) => {

        param.map((city: City) => {
          expect(city).toEqual(jasmine.any(City));
          expect(['cityid_1', 'cityid_2']).toContain(city.id);
          expect(['cityname_1', 'cityname_2']).toContain(city.name);

        });
      });
  }));

  it('should be added a city', inject([CityListService], (service: CityListService) => {
    service.addCity('city_1')
      .then((param) => {
        expect(uploadDbEntrySpy).toHaveBeenCalledWith('/cities', {name: 'city_1'});
      });
  }));

  it('should be removed a city from CityCategory',
    inject([CityListService], (service: CityListService) => {
      service.removeCityFromCityCategory('city_id')
        .then((param) => {
          expect(removeDbEntrySpy).toHaveBeenCalledWith('/cityCategory/city_id');
        });
    }));

  it('should be removed a city from CategoryList',
    inject([CityListService], (service: CityListService) => {
      service.removeCityFromCategoryList('city_id');

      expect(removeDbEntrySpy).not.toHaveBeenCalledWith(
        '/categories/undefined/cityName');
      expect(removeDbEntrySpy).toHaveBeenCalledWith(
        '/categories/category_id_2/cityName');
      expect(removeDbEntrySpy).toHaveBeenCalledWith(
        '/categories/category_id_2/cityId');

    }));

  it('should be removed a category from CategoryList',
    inject([CityListService], (service: CityListService) => {
      service.removeCategoryFromCityCategoryList('category_1');

      expect(removeDbEntrySpy).toHaveBeenCalledWith(
        '/cityCategory/city_1/category_1');

    }));



});
