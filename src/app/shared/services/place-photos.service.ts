import {Injectable} from '@angular/core';
import {UUID} from 'angular2-uuid';
import {MdSnackBar, MdSnackBarConfig} from '@angular/material';

import {DbRepository} from '../../model/db.repository';
import {Place} from '../../model/place.model';
import {Photo} from '../../model/photo';

export enum PhotoType {Preview, Photo, ExtraPhoto}


@Injectable()
export class PlacePhotosService {

  private entryName: string = '/places';

  private getPreviewFilePath(id: string) {
    return '/images/places/' + id + '/preview.jpg';
  }

  private getPhotoFilePath(id: string, photoId: string) {
    return '/images/places/' + id + '/photos/' + photoId + '.jpg';
  }

  private getExtraPhotoFilePath(id: string, photoId: string) {
    return '/images/places/' + id + '/extraPhotos/' + photoId + '.jpg';
  }

  private getPreviewDbPath(id: string) {
    return 'places/' + id + '/preview';
  }

  private getPhotoDbPath(id: string, photoId: string) {
    return 'places/' + id + '/photos/' + photoId;
  }

  private getExtraPhotoDbPath(id: string, photoId: string) {
    return 'places/' + id + '/extraPhotos/' + photoId;
  }

  constructor(private db_rep: DbRepository) {

  }

  /* -- Add photo -- */


  public addPreview(place: Place, photo: Photo) {
    if (!!place.previewFile.file.image) {
      return this.db_rep.uploadFileFromString(
        this.getPreviewFilePath(place.getId()),
        photo.file.image);
    }
    return new Promise((resolve, reject) => resolve());
  }

  public addPhotoListOnServer(place: Place, photoList: Photo[]) {
    return Promise.all(photoList
      .filter((photo) => !!photo.file.image)
      .map((photo) => this.addPhoto(place, photo)));
  }

  public addExtraPhotoListOnServer(place: Place, extraPhotoList: Photo[]) {
    return Promise.all(extraPhotoList
      .filter((photo) => !!photo.file.image)
      .map((photo) => this.addExtraPhoto(place, photo)));
  }


  public addPhoto(place: Place, photo: Photo) {
    let photoId = UUID.UUID();

    return this.db_rep.uploadFileFromString(
      this.getPhotoFilePath(place.getId(), photoId),
      photo.file.image)
      .then(
        snapshot => {
          photo.id = photoId;

          let u = {};
          u[place.getId()] = place.toJson();
          this.db_rep.updateDbEntry('/places', u);

        });
  }


  public addExtraPhoto(place: Place, photo: Photo) {
    let photoId = UUID.UUID();

    return this.db_rep.uploadFileFromString(
      `/images/places/${place.getId()}/extraPhotos/${photoId}.jpg`,
      photo.file.image)
      .then(
        snapshot => {
          photo.id = photoId;

          let u = {};
          u[place.getId()] = place.toJson();
          this.db_rep.updateDbEntry('/places', u);

        });
  }

  /* -- Remove photo -- */

  private removeImage(filepath, removeFromDb, place: Place, photoId: string = '') {
    return this.db_rep.deleteFile(filepath(place.getId(), photoId))
      .then(
        snapshot => {
          removeFromDb(place.getId(), photoId);
        }
      )
  }

  public removePreview = this.removeImage
    .bind(this, this.getPreviewFilePath, (placeId, photoId) => {
      this.db_rep.removeDbEntry(this.getPreviewDbPath(placeId));
    });

  public removePhoto = this.removeImage
    .bind(this, this.getPhotoFilePath, (placeId, photoId) => {
      this.db_rep.removeDbEntry(this.getPhotoDbPath(placeId, photoId));
    });

  public removeExtraPhoto = this.removeImage
    .bind(this, this.getExtraPhotoFilePath, (placeId, photoId) => {
      this.db_rep.removeDbEntry(this.getExtraPhotoDbPath(placeId, photoId));
    });

  /* -- Get photo url -- */

  private getImageUrl(filepath, place: Place, photoId: string = '') {
    return this.db_rep.downloadFile(filepath(place.getId(), photoId));
  }

  public getPreviewUrl = this.getImageUrl
    .bind(this, this.getPreviewFilePath);

  public getPhotoUrl = this.getImageUrl
    .bind(this, this.getPhotoFilePath);

  public getExtraPhotoUrl = this.getImageUrl
    .bind(this, this.getExtraPhotoFilePath);


}
