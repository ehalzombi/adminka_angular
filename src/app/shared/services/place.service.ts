import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/take';

import * as _ from 'lodash';

import {DbRepository} from "../../model/db.repository";
import {Place} from "../../model/place.model";

@Injectable()
export class PlaceService {

  private categoryPlacesPath() {
    return '/categoryPlaces';
  }

  private placesPath() {
    return '/places';
  }

  private placeDetailPath(id: string) {
    return `${this.placesPath()}/${id}`;
  }

  private categoryPath(id: string) {
    return '/categoryPlaces/' + id;
  }

  private categoryPlacePath(categoryId: string, placeId: string) {
    return '/categoryPlaces/' + categoryId + '/' + placeId;
  }

  private menuFilePath(id: string, ext: string) {
    return '/files/places/' + id + '/menu.' + ext;
  }

  private menuDbPath(id: string) {
    return '/places/' + id + '/menu';
  }

  private categoriesDbPath(id: string) {
    return '/places/' + id + '/categories';
  }

  constructor(private dbService: DbRepository) {
  }

  public getPlaceList() {
    return this.dbService.subscribeData(this.placesPath())
      .take(1)
      .map((response) => {
        return Object.keys(response)
          .filter((key) => !!response[key])
          .map((key) => {
            let p = new Place(response[key]);
            p.setId(key);
            return p;
          })
      })
  }

  public getPlaceById(id: string) {
    return this.getPlaceList()
      .map((response: Place[]) => response.find(
        (place: Place) => place.getId() == id
      ))
  }

  public getOrderedCategoryList(place: Place) {
    return this.dbService.subscribeData(this.categoryPlacesPath())
      .take(1)
      .map(
        (param) => (<any>Object).keys(param)
          .filter((key) => param[key] != undefined)
          .map((key) => new Object({
            category_id: key,
            place_order: param[key][place.getId()],
          }))
          .reduce((acc, iter) => {
            acc[iter['category_id']] = iter['place_order'];
            return acc;
          }, {}))
  }

  public addPlaceToCategoryPlaces(place: Place, categories) {

    this.setCategoryPlaces(categories, place);

    return this.dbService.subscribeData(this.categoryPlacesPath())
      .take(1)
      .mergeMap((param, index) => Observable.from(
        _.difference(
          Object.keys(param),
          Object.keys(categories)
        )
          .filter((key) => param[key] != undefined)
          .map((key) => {
            return new Object({
              category_id: key,
              place_id: Object.keys(param[key]).find(
                (place_key) => place_key == place.getId()
              ),
            });
          })
      ))
      .subscribe((param) => {
          if (param != undefined) {
            return this.dbService.removeDbEntry(
              this.categoryPlacePath(param['category_id'], param['place_id'])
            )
          }
        }
      )
  }

  private setCategoryPlaces(categories, place) {
    return Object.keys(categories)
      .filter((key) => categories[key] != undefined)
      .map((key) => new Object({
        category_id: key,
        place_order: categories[key],
      }))
      .map((param) => {
          return this.dbService.setDbEntry(
            this.categoryPlacePath(param['category_id'], place.getId()), param['place_order']
          );
        }
      );
  }

  public removePlaceFromCategoryPlaces(place_id: string) {
    this.dbService.subscribeData(this.categoryPlacesPath())
      .take(1)
      .mergeMap((param, index) => Observable.from(
        Object.keys(param)
          .filter((key) => param[key] != undefined)
          .map((key) => new Object({
            category_id: key,
            place_id: Object.keys(param[key]).find(
              (place_key) => place_key == place_id
            ),
          }))
      ))
      .subscribe((param) => {
        this.dbService.removeDbEntry(
          this.categoryPlacePath(param['category_id'], param['place_id'])
        );
      });
  }

  public addMenuFile(place: Place, file: File, afterAddingCallback = () => {
                     }) {
    if (place.menu) {
      this.removeMenuFile(place);
    }
    let ext = file.name.split('.').pop();
    place.menu = ext;

    return this.dbService.uploadFile(this.menuFilePath(place.getId(), ext), file)
      .then((param) => {
        this.dbService.setDbEntry(this.menuDbPath(place.getId()), ext);
        afterAddingCallback();
      });
  }

  public removeMenuFile(place: Place, afterRemovingCallback = () => {
                        }) {
    return this.dbService.deleteFile(this.menuFilePath(place.getId(), place.menu))
      .then((param) => {
        this.dbService.removeDbEntry(this.menuDbPath(place.getId()));
        afterRemovingCallback();
      })
  }

}
