import {TestBed, inject} from '@angular/core/testing';

import {Observable} from 'rxjs';

import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

import {PlaceService} from './place.service';
import {DbRepository} from "../../model/db.repository";

import {ApiMockService} from './tests/api-mock.service';
import {PlacePhotosService} from "./place-photos.service";
import {Photo} from "../../model/photo";

describe('PlaceService', () => {


  let uploadDbEntrySpy = jasmine.createSpy('uploadDbEntry');
  let setDbEntrySpy = jasmine.createSpy('setDbEntry');
  let removeDbEntrySpy = jasmine.createSpy('removeDbEntry');

  let deleteFileSpy = jasmine.createSpy('deleteFile').and.callFake((path: string) => {
    return new Promise((resolve, reject) => {
      resolve();
    });
  });
  let uploadFileSpy = jasmine.createSpy('uploadFile').and.callFake((path: string, file: File) => {
    return new Promise((resolve, reject) => {
      resolve();
    });
  });
  let uploadFileFromStringSpy = jasmine.createSpy('uploadFileFromString').and.callFake((path: string, file: File) => {
    return new Promise((resolve, reject) => {
      resolve();
    });
  });

  let dbStub = {
    uploadDbEntry: uploadDbEntrySpy,
    setDbEntry: setDbEntrySpy,
    removeDbEntry: removeDbEntrySpy,

    deleteFile: deleteFileSpy,
    uploadFile: uploadFileSpy,
    uploadFileFromString: uploadFileFromStringSpy,
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: DbRepository, useValue: dbStub},
        PlacePhotosService]
    });
    uploadDbEntrySpy.calls.reset();
    setDbEntrySpy.calls.reset();
    removeDbEntrySpy.calls.reset();

    uploadFileSpy.calls.reset();
    uploadFileFromStringSpy.calls.reset();
    deleteFileSpy.calls.reset();

  });

  it('should be created', inject([PlacePhotosService], (service: PlacePhotosService) => {
    expect(service).toBeTruthy();
  }));

  it('should be added a preview', inject([PlacePhotosService], (service: PlacePhotosService) => {
    let photo = new Photo();
    photo.file.image = 'image';

    let place = ApiMockService.place();
    place.previewFile.file.image = 'image';

    service.addPreview(place, photo);

    expect(uploadFileFromStringSpy).toHaveBeenCalledWith(
      '/images/places/place_id/preview.jpg',
      photo.file.image);
  }));

});
