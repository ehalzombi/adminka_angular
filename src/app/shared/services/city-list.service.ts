import {Injectable} from '@angular/core';

import {Observable} from "rxjs/Observable";
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/take';

import {DbRepository} from '../../model/db.repository';
import {City} from '../../model/city.model';
import {Category} from '../../model/category.model';

@Injectable()
export class CityListService {

  private cityListPath() {
    return '/cities';
  }

  private cityDetailPath(id: string) {
    return `/cities/${id}`;
  }

  private cityCategoryPath() {
    return '/cityCategory';
  }

  private cityCategoryCityDetailPath(city_id: string) {
    return `/cityCategory/${city_id}`;
  }

  private cityCategoryDetailPath(city_id, category_id) {
    return `/cityCategory/${city_id}/${category_id}`;
  }

  constructor(private db: DbRepository) {

  }

  public getCityObjects() {
    return this.db.subscribeData(this.cityListPath())
      .map((response) => {
        return Object.keys(response)
          .filter((key) => response[key] != undefined)
          .map((key) => new City({
            name: response[key]['name'],
            id: key,
          }))
      });
  }

  public addCity(cityname: string) {
    return this.db.uploadDbEntry(this.cityListPath(), {'name': cityname});
  }

  public removeCity(city_id: string) {
    this.removeCityFromCategoryList(city_id);
    this.removeCityFromCityCategory(city_id);
    this.removeCityFromCityList(city_id);
  }

  public removeCityFromCityCategory(city_id: string) {
    return this.db.removeDbEntry(
      this.cityCategoryCityDetailPath(city_id)
    );
  }

  public removeCityFromCityList(city_id: string) {
    return this.db.removeDbEntry(
      this.cityDetailPath(city_id)
    );
  }

  public removeCityFromCategoryList(city_id) {
    return this.db.subscribeData('/categories')
      .take(1)
      .mergeMap((param, index) => Observable.from(
        Object.keys(param)
          .filter((key) => param[key] != undefined)
          .map((key) => new Object({
            category_id: key,
            category: param[key],
          }))
      ))
      .map((param) => new Object({
          category_id: param['category_id'],
          city_id: param['category']['cityId']
        })
      )
      .filter((param) => param['city_id'] == city_id)
      .subscribe((param) => {
        this.db.removeDbEntry(`/categories/${param['category_id']}/cityName`);
        this.db.removeDbEntry(`/categories/${param['category_id']}/cityId`);
      })
  }

  public removeCategoryFromCityCategoryList(category_id: string) {
    this.db.subscribeData(this.cityCategoryPath())
      .take(1)
      .mergeMap((param, index) => Observable.from(
        Object.keys(param)
          .filter((key) => param[key] != undefined)
          .map((key) => new Object({
            city_id: key,
            category_list: param[key]
          })))
      )
      .mergeMap((param, index) => Observable.from(
        Object.keys(param['category_list'])
          .filter((key) => param['category_list'][key] != undefined)
          .map((key) => new Object({
            city_id: param['city_id'],
            category_id: key
          }))
      ))
      .filter((param) => param['category_id'] == category_id)
      .subscribe((response) => {
        this.db.removeDbEntry(
          this.cityCategoryDetailPath(response['city_id'], response['category_id'])
        );

      })
  }

  public saveCityCategory(category: Category) {
    console.log('category', category);

    this.db.setDbEntry(
      this.cityCategoryDetailPath(category.cityId, category.id), category.order
    ).then(() =>
      this.db.subscribeData(this.cityCategoryPath())
        .take(1)
        .mergeMap((param, index) => Observable.from(
          Object.keys(param)
            .filter((key) => param[key] != undefined)
            .map((key) => new Object({
              city_id: key,
              category_list: param[key]
            })))
        )
        .mergeMap((param, index) => Observable.from(
          Object.keys(param['category_list'])
            .filter((key) => param['category_list'][key] != undefined)
            .map((key) => new Object({
              city_id: param['city_id'],
              category_id: key,
            }))
        ))
        .filter((param) => param['category_id'] == category.id
        && param['city_id'] !== category.cityId)
        .subscribe((response) => {
          this.db.removeDbEntry(
            this.cityCategoryDetailPath(response['city_id'], response['category_id'])
          );
        }));
  }

  public updateCityName(city: City) {
    this.db.subscribeData('/categories')
      .take(1)
      .mergeMap((param, index) => Observable.from(
        Object.keys(param)
          .filter((key) => param[key] != undefined)
          .map((key) => new Object({
            category_id: key,
            category: param[key],
          }))
      ))
      .map((param) => new Object({
          category_id: param['category_id'],
          city_id: param['category']['cityId']
        })
      )
      .filter((param) => param['city_id'] == city.id)
      .subscribe((param) => {
        this.db.updateDbEntry(`/categories/${param['category_id']}`, {'cityName': city.name});
      });

    return this.db.updateDbEntry(this.cityDetailPath(city.id), city.toJson());
  }

}
