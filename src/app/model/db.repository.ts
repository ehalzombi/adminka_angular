import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/take';
import {Place} from "./place.model";
import {AngularFireDatabase, FirebaseObjectObservable} from 'angularfire2/database';
import "rxjs/add/observable/from";
import * as firebase from 'firebase';
import {AngularFireAuth} from "angularfire2/auth";

@Injectable()
export class DbRepository {
  private storageRef;


  constructor(private db: AngularFireDatabase) {
    this.storageRef = firebase.storage().ref();
  }

  subscribeData(entryName: string) {
    return this.db.object(entryName);
  }

  takeData(entryName: string) {
    return this.db.object(entryName,{ preserveSnapshot: true }).take(1);
  }
  uploadFile(filepath: string, file: any) {
    return this.storageRef.child(filepath).put(file);
  }

  uploadFileFromString(filepath: string, file: any) {
    return this.storageRef.child(filepath).putString(file, 'data_url');
  }

  downloadFile(filepath: string) {
    return this.storageRef.child(filepath).getDownloadURL();
  }

  deleteFile(filepath: string) {
    return this.storageRef.child(filepath).delete();

  }

  uploadDbEntry(entryName: string, data: any) {
    return this.db.list(entryName).push(data);
  }

  setDbEntry(entryName: string, data: any) {
    return this.db.object(entryName).set(data);
  }

  updateDbEntry(entryName: string, data: any) {
    return this.db.object(entryName).update(data);
  }

  removeDbEntry(entryName: string) {
    return this.db.object(entryName).remove();

  }

}
