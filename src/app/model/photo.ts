export class Photo {
  public serverUrl: string = '';
  public image: any = '';
  public id: string = '';
  public file: any = {};
  public type: string = '';
  public name: string = '';
}
