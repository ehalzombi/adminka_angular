export class City {
  public id: string;
  public name: string;
  public active: boolean = false;
  public onEdit: boolean = false;

  constructor (obj) {
    this.id = obj['id'];
    this.name = obj['name'];
  }

  public toJson() {
    return {
      name: this.name,
    }
  }
}
