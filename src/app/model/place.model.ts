import {Photo} from './photo';

export class Place {
  get menu(): string {
    return this._menu;
  }

  set menu(value: string) {
    this._menu = value;
  }

  get menuFile(): File {
    return this._menuFile;
  }

  set menuFile(value: File) {
    this._menuFile = value;
  }

  get previewFile(): Photo {
    return this._previewFile;
  }

  set previewFile(value: Photo) {
    this._previewFile = value;
  }

  get categoriesIdList(): string[] {
    return this._categoriesIdList;
  }

  set categoriesIdList(value: string[]) {
    this._categoriesIdList = value;
  }

  get preview(): string {
    return this._preview;
  }

  set preview(value: string) {
    this._preview = value;
  }

  get photoList(): Photo[] {
    return this._photoList;
  }

  set photoList(value: Photo[]) {
    this._photoList = value;
  }

  get extraPhotoList(): Photo[] {
    return this._extraPhotoList;
  }

  set extraPhotoList(value: Photo[]) {
    this._extraPhotoList = value;
  }

  private out: any;

  private id: string;
  name: string;
  active: boolean;
  description: string;
  check: string;
  extraTitle: string;
  reserveDays: string;
  reserveInterval: string;
  hours: object;
  services: object;
  address: string;
  website: string;
  greet: string;
  phone: string;
  customer: boolean;

  public relatedCategoriesList: any[] = [];


  private admin: object;

  private _preview: string;
  private _previewFile: Photo = new Photo();
  private _photoList: Photo[] = [];
  private _extraPhotoList: Photo[] = [];
  private _categoriesIdList: string[];
  private _menu: string;
  private _menuFile: File;


  constructor(json_obj: any = {}) {
    this.out = {};
    this.menu = '';
    this.id = '';
    this.active = false;
    this.name = '';
    this.extraTitle = '';
    this.description = '';
    this.reserveInterval = '';
    this.reserveDays = '';
    this.check = '';
    this.address = '';
    this.website = '';
    this.greet = '';
    this.phone = '';
    this.preview = '';
    this.admin = {};
    this.services = {};
    this.customer = false;


    this.hours = {};
    for (let d in this.getWeekDays) {
      this.hours[d] = {};
      for (let t in this.getWeekTimes) {
        this.hours[d][t] = 0;
      }
    }

    for (let property in json_obj) {
      this[property] = json_obj[property];
    }

    for (let s in this.getServices) {
      this.services[s] = this.services[s] ? true : false;
    }

    if (this.preview) {
      this.previewFile.type = this.preview;
    }

    for (let i in this['photos']) {
      let p = new Photo();
      p.id = i;
      p.type = this['photos'][i];
      this.photoList.push(p);
    }

    for (let i in this['extraPhotos']) {
      let p = new Photo();
      p.id = i;
      p.type = this['extraPhotos'][i];
      this.extraPhotoList.push(p);
    }

    this.categoriesIdList = [];
    for (let i in this['categories']) {
      this.categoriesIdList.push(i);
    }
  }

  // -- Set

  setId(id: string) {
    this.id = id;
  }

  getId() {
    return this.id;
  }

  get getWeekDays(): any {
    return {
      'mon': 'Понедельник',
      'tue': 'Вторник',
      'wed': 'Среда',
      'thu': 'Четверг',
      'fri': 'Пятница',
      'sat': 'Суббота',
      'sun': 'Воскресенье'
    };
  }

  get getWeekTimes(): any {
    return {
      'openingTime': 'Время открытия',
      'closingTime': 'Время закрытия'
    }
  }

  get getServices(): any {
    return {
      'wifi': 'Wi-Fi',
      'xbox': 'X-Box',
      'playstation': 'PlayStation',
      'bar': 'Бар',
      'ownAlco': 'Со своим алкоголем',
      'karaoke': 'Караоке',
      'liveMusic': 'Живая музыка',
      'standUp': 'Стендап',
      'kidAnimation': 'Анимация для детей',
      'liveSport': 'Спортивные трансляции',
      'vip': 'VIP зоны',
      'boardGame': 'Настольные игры',
      'dj': 'Dj',
      'cuisine': 'Кухня',
      'cItalian': 'Итальянская кухня',
      'cSpanish': 'Испанская кухня',
      'cFrench': 'Французкая кухня',
      'cGeorgian': 'Грузинская кухня',
      'cGerman': 'Немецкая кухня',
      'cMexican': 'Мексиканская кухня',
      'cEnglish': 'Английская кухня',
      'cEuropean': 'Европейская кухня',
      'cEastern': 'Восточная кухня',
      'businessLunch': 'Бизнес ланч',
      'dance': 'Танцпол',
      'deal': 'Деловая встреча',
      'family': 'Семейный ужин',
      'kidCinema': 'Детское меню',
      'showtime': 'Шоу-программа',
      'hookah': 'Кальян',
    };
  }

  toJson() {
    this.out = {
      'active': this.active,
      'name': this.name,
      'description': this.description,
      'extraTitle': this.extraTitle,
      'check': parseInt(this.check),
      'reserveDays': parseInt(this.reserveDays),
      'reserveInterval': parseInt(this.reserveInterval),
      'address': this.address,
      'website': this.website,
      'greet': this.greet,
      'phone': this.phone,
      'customer': this.customer,
      'menu': this.menu,

      'hours': {},
      'services': {},
      'photos': {},
      'extraPhotos': {},
      'categories': {},
      'admin': this.admin,
    };

    if (this.previewFile.type) {
      this.out['preview'] = this.previewFile.type;
    }

    for (let d in this.getWeekDays) {
      this.out['hours'][d] = {};
      for (let t in this.getWeekTimes) {
        this.out['hours'][d][t] = parseInt(this.hours[d][t]);
      }
    }

    for (let s in this.getServices) {
      if(this.services[s]) {
        this.out['services'][s] = this.services[s];
      }
    }

    for (let c in this.categoriesIdList) {
      this.out['categories'][this.categoriesIdList[c]] = true;
    }

    for (let i in this.photoList) {
      if (this.photoList[i].id) {
        this.out['photos'][this.photoList[i].id] = this.photoList[i].type;

      }
    }

    for (let i in this.extraPhotoList) {
      if (this.extraPhotoList[i].id) {
        this.out['extraPhotos'][this.extraPhotoList[i].id] = this.extraPhotoList[i].type;

      }
    }

    return this.out;
  }
}
