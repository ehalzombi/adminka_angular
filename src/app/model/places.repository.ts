import {Injectable} from "@angular/core";
import {MdSnackBar, MdSnackBarConfig} from '@angular/material';

import {Place} from "./place.model";
import {DbRepository} from "./db.repository";
import {PlacePhotosService, PhotoType} from "../shared/services/place-photos.service";
import {PlaceUserPathes} from "./../accesses-dialog/accesses-dialog.service";

import 'rxjs/add/observable/of'
import 'rxjs/add/observable/from'

import 'rxjs/add/operator/take';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/scan';

import {Observable} from "rxjs";

@Injectable()
export class PlaceRepository {
  private places: Place[] = [];
  private entryName: string = '/places';

  constructor(private db_rep: DbRepository,
              private snackbar: MdSnackBar,
              private photoService: PlacePhotosService) {

    this.getPlacesList()
      .subscribe(lst => {
        this.places = lst;
      })
  }

  private categoriesPath() {
    return '/categoryPlaces';
  }

  private categoryPath(id: string) {
    return '/categoryPlaces/' + id;
  }

  private categoryPlacePath(categoryId: string, placeId: string) {
    return '/categoryPlaces/' + categoryId + '/' + placeId;
  }

  private menuFilePath(id: string, ext: string) {
    return '/files/places/' + id + '/menu.' + ext;
  }

  private menuDbPath(id: string) {
    return '/places/' + id + '/menu';
  }

  private categoriesDbPath(id: string) {
    return '/places/' + id + '/categories';
  }

  public getOrderedCategoriesList(place: Place) {
    /* place id
     * {
     *    category_id: number,
     *    category_id: number
     * }
     *
     * */
    return this.db_rep.subscribeData(this.categoriesPath())
      .mergeMap((param, index) => {
        return Observable.from(
          Object.keys(param)
            .map(cat_id => {
              return {
                cat_id: cat_id,
                cat_val: param[cat_id]
              }
            })
        );
      })
      .filter((val) => {
        return val['cat_val'][place.getId()] != undefined;
      })
      .scan((acc, iter) => {
        acc[iter['cat_id']] = iter['cat_val'][place.getId()];
        return acc;
      }, {});
  }

  //+
  public addMenuFile(place: Place, menuFile: File) {
    if (place.menu) {
      this.db_rep.deleteFile(this.menuFilePath(place.getId(), place.menu))
    }

    let ext = menuFile.name.split('.').pop();
    place.menu = ext;

    let config = new MdSnackBarConfig();
    config.duration = 500;

    return this.db_rep.uploadFile(this.menuFilePath(place.getId(), ext), menuFile)
      .then(
        snapshot => {
          this.db_rep.setDbEntry(this.menuDbPath(place.getId()), ext)
            .then(
              snashot => {
                this.snackbar.open('Меню загружено', 'Закрыть', config);
              }
            );

        });
  }

  // +
  public removeMenuFile(place: Place) {
    return this.db_rep.removeDbEntry(this.menuDbPath(place.getId()))
      .then((snapshot) => {
        this.db_rep.deleteFile(this.menuFilePath(place.getId(), place.menu));
        place.menu = '';
        //alert('Menu removed!');
      })
      .catch((err) => {
        alert('Меню не удалено')
      })
  }


  // +
  public removeFromCategoryList(id: string) {
    let listToRemove: string[] = [];
    return this.db_rep.subscribeData(this.categoriesPath())
      .take(1)
      .forEach((categoryIdList) => {
        let catIdList = Object.keys(categoryIdList);
        for (let i = 0; i < catIdList.length; ++i) {
          if (catIdList[i] != '$value')
            this.db_rep.subscribeData(this.categoryPath(catIdList[i]))
              .forEach((placeIdList) => {
                let plIdList = Object.keys(placeIdList);
                for (let j = 0; j < plIdList.length; ++j) {
                  if (plIdList[j] == id && plIdList[j] != '$value') {
                    listToRemove.push(this.categoryPlacePath(catIdList[i], plIdList[j]));
                  }
                }
              })
        }
      })
      .then(snapshot => {
        listToRemove.forEach((toRemove) => {
          this.db_rep.removeDbEntry(toRemove);
        })
      });
  }

  public updatePlace(place: Place) {
    let update = {};
    update[place.getId()] = place.toJson();

    return this.db_rep.updateDbEntry(this.entryName, update);
  }

  uploadImageFileList(photoType: PhotoType, place: Place, files: any[]) {
    return new Promise((resolve, reject) => {
      for (let i = 0; i < files.length; ++i) {

        switch (photoType) {
          case PhotoType.Photo:
            this.photoService.addPhoto(place, files[i]);
            break;
          case PhotoType.ExtraPhoto:
            this.photoService.addExtraPhoto(place, files[i]);
            break;
        }
      }
      resolve();

    })
  }

  public uploadPreview = this.photoService.addPreview;

  public uploadPhotoFileList = this.uploadImageFileList
    .bind(this, PhotoType.Photo);

  public uploadExtraPhotoFileList = this.uploadImageFileList
    .bind(this, PhotoType.ExtraPhoto);

  public getPreviewUrl = this.photoService.getPreviewUrl;

  public getPhotoUrl = this.photoService.getPhotoUrl;

  public getExtraPhotoUrl = this.photoService.getExtraPhotoUrl;

  public getMenuFileUrl(place: Place) {
    return this.db_rep.downloadFile(this.menuFilePath(place.getId(), place.menu));
  }

  // +
  public addPlace(place: Place) {
    this.places = [];

    return this.db_rep.uploadDbEntry('/places', place.toJson());
  }


  public removePlace(place: Place) {
    return this.db_rep.removeDbEntry('' + this.entryName + '/' + place.getId())
      .then(snapshot => {
        this.db_rep.subscribeData(PlaceUserPathes.userPath())
          .subscribe(usrList => {
            for (let usr in usrList) {
              if (usr != '$value') {
                this.db_rep.subscribeData(PlaceUserPathes.userPlaceListPath(usr))
                  .subscribe(
                    placeIdList => {
                      for (let placeId in placeIdList) {
                        if (placeId == place.getId() && placeId != '$value') {
                          this.db_rep.removeDbEntry(PlaceUserPathes.userPlacePath(usr, placeId));
                        }
                      }
                    })
              }
            }
          })
      })
      .then(snapshot => {
        this.removeFromCategoryList(place.getId());
      })
      .then((snapshot) => {
        if (place.preview) {
          this.removePreview(place);
        }

        place.photoList
          .forEach((ph) => this.removePhoto(place, ph.id));

        place.extraPhotoList
          .forEach((ph) => this.removeExtraPhoto(place, ph.id));
      })
      .then(
        snapshot => {
          if (place.menu) {
            this.removeMenuFile(place);
          }
        }
      )
      .then(
        snapshot => {
          this.removeFromCategoryList(place.getId());
        }
      );
  }

  public removePreview = this.photoService.removePreview;

  public removePhoto = this.photoService.removePhoto;

  public removeExtraPhoto = this.photoService.removeExtraPhoto;


  // +
  public getPlace(id: string): Place {
    return this.places.find(p => p.getId() == id);
  }

  // +
  public getPlacesList() {
    return this.db_rep.subscribeData('/places')
      .map(x => {
        let places = [];
        for (let key in x) {
          if (key != '$value') {
            let p = new Place(x[key]);
            p.setId(key);
            places.push(p)
          }
        }
        return places;
      });
  }

  getEmptyPlace() {
    return new Place({});
  }
}
