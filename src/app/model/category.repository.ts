import {Injectable} from "@angular/core";
import {MdSnackBar, MdSnackBarConfig} from '@angular/material';

import {Observable} from "rxjs/Observable";
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/scan';

import {DbRepository} from "./db.repository";
import {Category} from "./category.model";
import {City} from './city.model';
import {Photo} from "./../model/photo";
import {CityListService} from "../shared/services/city-list.service";


@Injectable()
export class CategoryRepository {

  categories: Category[] = [];
  entryName: string = '/categories';

  private getPreviewFilePath(id: string) {
    return '/images' + this.entryName + '/' + id + '/preview.jpg';
  }

  private getPreviewDbPath(id: string) {
    return '/categories/' + id + '/preview';
  }

  private categoryPath(id: string) {
    return '/categoryPlaces/' + id;
  }


  private getCategoryEntryPath(id: string) {
    return '/categories/' + id;
  }


  constructor(private db_rep: DbRepository,
              private cityListService: CityListService,
              private snackbar: MdSnackBar) {

    this.getCategoriesList()
      .subscribe((snap) => {
        this.categories = snap;
      });

  }

  addCategory(category: Category) {
    return this.db_rep.uploadDbEntry(this.entryName,
      category.toJson());
  }

  updateCategory(category: Category) {
    let updation = {};
    updation[category.id] = category.toJson();

    return this.db_rep.updateDbEntry(this.entryName, updation);
  }

  uploadPreviewFile(id: string, file: Photo) {
    let bar = this.snackbar.open(`${file.name} загружается`, 'Закрыть');

    return this.db_rep.uploadFileFromString(this.getPreviewFilePath(id), file.file.image)
      .then(snapshot => {
        bar.dismiss();

      });
  }

  getCategoryById(id: string) {
    return this.categories.find(cat => cat.id == id);
  }

  getCategoriesList() {
    return this.db_rep.subscribeData(this.entryName)
      .map(x => {
        let categories = [];

        for (let key in x) {
          if (key != '$value') {
            let c = new Category(x[key]);
            c.id = key;
            this.getCategoryPreviewUrlById(c.id).then(
              url => {
                c.previewUrl = url;
              },
              err => c.previewUrl = ''
            );
            categories.push(c);

          }
        }
        return categories;
      })
  }

  getCategoryPreviewUrlById(id: string) {
    return this.db_rep.downloadFile(this.getPreviewFilePath(id));
  }

  removeCategory(id: string) {
    return this.db_rep.removeDbEntry(this.getCategoryEntryPath(id))
      .then(
        snapshot => {
          this.db_rep.deleteFile(this.getPreviewFilePath(id));
          this.cityListService.removeCategoryFromCityCategoryList(id);
        },
        err => alert('File doesnot exist')
      )
  }


  public removeFromCategoryList(id: string) {
    return this.db_rep.removeDbEntry(this.categoryPath(id));

  }

}
