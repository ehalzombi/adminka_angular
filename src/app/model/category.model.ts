import {Photo} from './photo';
import {City} from './city.model';



export class Category {
  get active(): boolean {
    return this._active;
  }

  set active(value: boolean) {
    this._active = value;
  }
  get previewUrl(): string {
    return this._previewUrl;
  }

  set previewUrl(value: string) {
    this._previewUrl = value;
  }
  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get announced(): boolean {
    return this._announced;
  }

  set announced(value: boolean) {
    this._announced = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get order(): number {
    return this._order;
  }

  set order(value: number) {
    this._order = value;
  }

  get previewFile(): Photo {
    return this._previewFile;
  }

  set previewFile(value: Photo) {
    this._previewFile = value;
  }

  get preview(): string {
    return this._preview;
  }

  set preview(value: string) {
    this._preview = value;
  }

  public cityList: City[] = [];

  public cityName: string;
  public cityId: string;

  private _id: string;
  private _announced: boolean;
  private _active: boolean;
  private _name: string;
  private _order: number;
  private _previewFile: Photo;
  private _preview: string;
  private _previewUrl: string;

  private _out: object = {};

  constructor(json_obj: any = {}) {
    this._id = '';
    this._announced = false;
    this._active = false;
    this._name = '';
    this._order = -1;
    this._previewFile = new Photo();
    this._preview = '';
    this.cityName = '';
    this.cityId = '';

    for (let property in json_obj) {
      this[property] = json_obj[property];
    }
  }

  toJson() {
    this._out = {
      'announced': this._announced,
      'active': this._active,
      'name': this._name,
      'order': this._order,
      'preview': this._preview,

      'cityName': this.cityName,
      'cityId': this.cityId,
    };

    return this._out;
  }


}
