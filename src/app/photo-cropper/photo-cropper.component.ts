import {Component, Renderer2, AfterViewInit, ViewChild, Inject, ElementRef} from '@angular/core';
import {ImageCropperComponent, CropperSettings, Bounds} from 'ng2-img-cropper';
import {Ng2ImgToolsService} from 'ng2-img-tools';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';

import {Photo} from '../model/photo';


@Component({
  selector: 'app-photo-cropper',
  templateUrl: './photo-cropper.component.html',
  styleUrls: ['./photo-cropper.component.css']
})
export class PhotoCropperComponent implements AfterViewInit {
  public cropperSettings: CropperSettings = new CropperSettings();
  public photo: Photo = new Photo();

  @ViewChild('cropper')
  public cropper: ImageCropperComponent;

  public croppedX: number = 0;
  public croppedY: number = 0;
  public croppedHeight: number = 100;
  public croppedWidth: number = 100;

  public imageData: any = {};

  public imageWidth: number =  900;
  public imageHeight: number = 500;

  constructor(@Inject(MD_DIALOG_DATA)
              private data: any,
              public dialogRef: MdDialogRef<PhotoCropperComponent>,
              private renderer: Renderer2) {

    this.cropperSettings.noFileInput = true;
    this.cropperSettings.preserveSize = false;
    this.cropperSettings.canvasWidth = this.imageWidth;
    this.cropperSettings.canvasHeight = this.imageHeight;

    if(this.data.width && this.data.height) {

      this.cropperSettings.croppedWidth = this.data.width ? this.data.width : 810;
      this.cropperSettings.croppedHeight = this.data.height ? this.data.height : 360;
      this.cropperSettings.width = this.data.width ? this.data.width : 810;
      this.cropperSettings.height = this.data.height ? this.data.height : 360;
    } else {

      this.cropperSettings.keepAspect = false;

      this.cropperSettings.width = this.imageWidth;
      this.cropperSettings.height = this.imageHeight;
      this.cropperSettings.croppedWidth = this.imageWidth;
      this.cropperSettings.croppedHeight = this.imageHeight;

    }
    this.cropperSettings.fileType = 'image/jpeg';

  }

  public savePhoto(photo: Photo) {

    this.dialogRef.close(photo);
  }

  cropped(bounds: Bounds) {

    if(!this.data.width && !this.data.height) {
      this.croppedX = bounds.left;
      this.croppedY = bounds.top;

      this.croppedHeight = bounds.bottom - bounds.top;
      this.croppedWidth = bounds.right - bounds.left;

      let canvas = this.renderer.createElement('canvas');
      let img = this.renderer.createElement('img');


      this.renderer.setAttribute(img, 'src', this.data.reader.result);
      this.renderer.setAttribute(img, 'width', String(this.imageWidth));
      this.renderer.setAttribute(img, 'height', String(this.imageHeight));

      this.renderer.setAttribute(canvas, 'width', this.croppedWidth.toString());
      this.renderer.setAttribute(canvas, 'height', this.croppedHeight.toString());

      let context: CanvasRenderingContext2D = canvas.getContext('2d');
      context.drawImage(img,
        this.croppedX, this.croppedY,
        this.croppedWidth, this.croppedHeight,
        0, 0,
        this.croppedWidth, this.croppedHeight,
      );

      this.photo.file.image = canvas.toDataURL('image/jpeg');

    }
  }

  ngAfterViewInit() {

    this.photo.image = new Image();
    this.photo.image.src = this.data.reader.result;
    this.photo.type = 'jpg';
    this.photo.name = this.data.file.name;
    this.photo.file = this.imageData;
    this.cropper.setImage(this.photo.image);
  }

}
