import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PhotoCropperComponent} from './photo-cropper.component';
import {ImageCropperModule, ImageCropperComponent} from 'ng2-img-cropper';
import {Ng2ImgToolsModule} from 'ng2-img-tools'

@NgModule({
  declarations: [
    PhotoCropperComponent,
  ],
  entryComponents: [
    PhotoCropperComponent
  ],
  imports: [
    CommonModule,
    ImageCropperModule,
    Ng2ImgToolsModule,
  ]
})
export class PhotoCropperModule {
}
