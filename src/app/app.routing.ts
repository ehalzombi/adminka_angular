import {Routes, RouterModule} from '@angular/router';
import {PlacesTableComponent} from './places-table/places-table.component';
import {NewPlaceComponent} from './new-place/new-place.component';
import {EditCategoryComponent} from './edit-category/edit-category.component';
import {CategoriesTableComponent} from './categories-table/categories-table.component';
import {CityListComponent} from './city-list/city-list.component';
import {LoginComponent} from './login/login.component';

import {AuthGuard} from './auth/auth.guard';


const appRoutes: Routes = [
  {
    path: 'places/edit/id/:id',
    component: NewPlaceComponent,
    canActivate: [AuthGuard],

  },
  {
    path: 'categories/edit/id/:id',
    component: EditCategoryComponent,
    canActivate: [AuthGuard],

  },
  {
    path: 'places',
    component: PlacesTableComponent,
    canActivate: [AuthGuard],

  },
  {
    path: 'categories',
    component: CategoriesTableComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'cities',
    component: CityListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '**',
    redirectTo: 'places',
    pathMatch: 'full'
  },
];

export const route = RouterModule.forRoot(appRoutes);
