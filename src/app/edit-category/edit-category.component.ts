import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {MdDialog, MdDialogConfig} from '@angular/material';

import 'rxjs/add/operator/take';

import {Category} from './../model/category.model';
import {City} from './../model/city.model';
import {CategoryRepository} from './../model/category.repository';
import {PhotoCropperComponent} from '../photo-cropper/photo-cropper.component';
import {Photo} from './../model/photo';
import {CityListService} from "../shared/services/city-list.service";


@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit, OnDestroy {

  public currentCategory: Category = new Category({});
  private cityListSubscription$;
  private categoryListSubscription$;

  constructor(private repository: CategoryRepository,
              private cityService: CityListService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private cropperDialog: MdDialog) {


    let id = this.activatedRoute.snapshot.params["id"];

    if (id) {
      this.categoryListSubscription$ = this.repository.getCategoriesList()
        .subscribe((cat) => {
          this.currentCategory = <Category>cat.find((x, i, arr) => {
            if (x['_id'] == id) {
              return x;
            }
          })


        })
    }

    // -- dbg
    this.cityListSubscription$ = this.cityService.getCityObjects()
      .subscribe((response) => {
        this.currentCategory.cityList = response || [];
      });

    // -- dbg

  }


  uploadCategory() {
    if (this.currentCategory.previewFile) {
      this.currentCategory.preview = 'jpg';
    }

    if (this.currentCategory.id) {
      this.repository.updateCategory(this.currentCategory)
        .then(
          (snapshot) => {


            if (this.currentCategory.previewFile.file.image != undefined) {
              this.repository.uploadPreviewFile(
                this.currentCategory.id, this.currentCategory.previewFile)
                .then(
                  url => {
                    this.currentCategory.previewFile.serverUrl = url;
                    this.router.navigate(['categories']);
                  })
            } else {
              this.router.navigate(['categories']);
            }

          });
      this.cityService.saveCityCategory(
        this.currentCategory
      );

    } else {
      this.repository.addCategory(this.currentCategory)
        .then((snapshot) => {
          this.currentCategory = this.repository.getCategoryById(snapshot.key);
          this.cityService.getCityObjects()
            .take(1)
            .subscribe((response) => {
              this.currentCategory.cityList = response;
            });
          this.router.navigate(['categories', 'edit', 'id', this.currentCategory.id]);
        });
    }
  }

  uploadPreview(event) {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();

      reader.onload = (e) => {
        let config = new MdDialogConfig();
        config.data = {
          width: 810,
          height: 360,
          reader: e.target,
          file: event.target.files[0]
        };

        let dialogRef = this.cropperDialog.open(PhotoCropperComponent, config)
          .afterClosed().subscribe(photo => {
            if (photo != undefined) {
              this.currentCategory.previewFile = photo;
            }
          });

      };
      reader.readAsDataURL(event.target['files'][0]);
    }
  }

  getPreviewUrl() {
    if (this.currentCategory.previewFile.file.image != undefined) {
      return this.currentCategory.previewFile.file.image;
    } else if (this.currentCategory.previewUrl) {
      return this.currentCategory.previewUrl;
    } else {
      return this.currentCategory.previewFile.serverUrl;
    }
  }

  public toggleCurrentCategoryActivityOrders() {
    if(this.currentCategory.active) {
      this.currentCategory.order = -1;
    } else {
      this.currentCategory.order = 0;
    }
    this.currentCategory.active = !this.currentCategory.active
  }

  public setCity(city: City) {
    this.currentCategory.cityName = city.name;
    this.currentCategory.cityId = city.id;
  }

  public removePreview() {
    this.currentCategory.previewFile = new Photo();
  }

  ngOnInit() {

    if (this.currentCategory.preview) {
      this.repository.getCategoryPreviewUrlById(this.currentCategory.id).then(
        url => {
          this.currentCategory.previewFile.serverUrl = url;
        },
        err => console.error(err)
      );
    }

  }

  ngOnDestroy() {
    this.cityListSubscription$.unsubscribe();
    if(this.categoryListSubscription$) {
      this.categoryListSubscription$.unsubscribe();
    }
  }

}
