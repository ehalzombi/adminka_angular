import {Component, OnInit} from '@angular/core';
import {AuthService} from './auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  public authenticated: any;

  constructor(public authService: AuthService,
              public router: Router) {
  }

  public logout() {
    this.router.navigate(['/login'])
      .then(snapshot => {
        this.authService.logout();
      });

  }

  public isAuthenticated() {
    return this.authenticated;
  }

  ngOnInit() {
    this.authenticated = this.authService.isAuthenticated()
      .subscribe(resp => {
        if(resp) {
          this.authenticated = !!resp;
        } else {
          this.authenticated = false;
        }
      });

  }

}
