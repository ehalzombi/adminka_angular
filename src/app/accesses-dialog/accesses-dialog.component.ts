import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {Component, OnInit, Inject, Optional} from '@angular/core';
import {Place} from './../model/place.model';

import {AccessesDialogService} from './accesses-dialog.service';

@Component({
  selector: 'app-accesses-dialog',
  templateUrl: './accesses-dialog.component.html',
  styleUrls: ['./accesses-dialog.component.css']
})
export class AccessesDialogComponent implements OnInit {

  private currentPlace: Place = new Place();
  private userList: User[] = [];

  constructor(@Optional()
              @Inject(MD_DIALOG_DATA)
              private activePlace: any,
              public dialogRef: MdDialogRef<AccessesDialogComponent>,
              private accessesDialogService: AccessesDialogService) {
    this.userList = [];
  }

  public addUserId(userId: string) {
    this.accessesDialogService
      .addUserId(this.currentPlace, userId);
  }

  public removeUserId(userId: string) {
    this.accessesDialogService
      .removeUserId(this.currentPlace, userId);
  }

  public getUserList() {
    return this.userList;
  }



  ngOnInit() {
    this.currentPlace = this.activePlace;

    this.accessesDialogService.getUsersList(this.currentPlace)
      .subscribe(
        snapshot => {
          this.userList = [];
          for(let userId in snapshot) {
            console.log(userId);
            if(userId != '$value') {
              let u = new User(userId);

              this.accessesDialogService.getUserEmail(userId)
                .subscribe(snapshot => {
                  u.email = snapshot['$value'] || 'Вход не выполнялся';
                });

              this.userList.push(u);
            }

          }
        }
      );

  }

}

class User {

  constructor(public userId: string = '') {}
  email: string = '';
}
