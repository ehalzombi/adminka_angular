import {NgModule} from '@angular/core';
import {AccessesDialogComponent} from './accesses-dialog.component';
import {AccessesDialogService} from './accesses-dialog.service';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    AccessesDialogComponent
  ],
  entryComponents: [
    AccessesDialogComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [
    AccessesDialogService
  ]
})
export class AccessesDialogModule {}
