import {Injectable} from '@angular/core';
import {Place} from '../model/place.model';
import {DbRepository} from '../model/db.repository';

export class PlaceUserPathes {
  public static placeUserEntriesListPath(id) {
    return '/places/' + id + '/admin';
  }

  public static placeUserEntryPath(id, userId) {
    return '/places/' + id + '/admin/' + userId;
  }

  public static userPlaceListPath(userId: string) {
    return '/adminPlaces/'+userId+'/';
  }

  public static userPlacePath(userId: string, placeId: string) {
    return '/adminPlaces/'+userId+'/'+placeId;
  }

  public static userPath() {
    return '/adminPlaces';
  }

  public static userEmailPath(userId: string) {
    return '/users/'+userId+'/email';
  }
}

@Injectable()
export class AccessesDialogService {

  constructor(private db_rep: DbRepository) {
  }

  public getUsersList(place: Place) {
    return this.db_rep.subscribeData(PlaceUserPathes.placeUserEntriesListPath(place.getId()));
  }

  public getAllUsersList() {
    return this.db_rep.subscribeData(PlaceUserPathes.userPath());
  }

  public getUserEmail(userId: string) {
    return this.db_rep.subscribeData(PlaceUserPathes.userEmailPath(userId));
  }

  public removeUserId(place: Place, userId: string) {
    return this.db_rep.removeDbEntry(PlaceUserPathes.placeUserEntryPath(place.getId(), userId))
      .then(snapshot => {
        this.db_rep.removeDbEntry(PlaceUserPathes.userPlacePath(userId, place.getId()))
      });
  }

  public addUserId(place: Place, userId: string) {
    return this.db_rep.setDbEntry(
      PlaceUserPathes.placeUserEntryPath(place.getId(), userId), true)
      .then(
        snapshot => {
          this.db_rep.setDbEntry(PlaceUserPathes.userPlacePath(
            userId, place.getId()), place.name);
        }
      )
  }

  public adminUserIdList() {
    return this.db_rep.subscribeData(PlaceUserPathes.userPath())
      .map(usr => Object.keys(usr))
  }

}
