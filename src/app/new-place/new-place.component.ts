import {Component, AfterViewInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {MdDialog, MdDialogConfig, MdSnackBar} from '@angular/material';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toArray';


import {Photo} from '../model/photo';
import {Place} from '../model/place.model';
import {Category} from '../model/category.model';
import {PlaceRepository} from '../model/places.repository';
import {CategoryRepository} from '../model/category.repository';
import {PhotoCropperComponent} from '../photo-cropper/photo-cropper.component';
import {PlacePhotosService} from "../shared/services/place-photos.service";
import {PlaceService} from "../shared/services/place.service";

@Component({
  moduleId: module.id,
  selector: 'app-new-place',
  templateUrl: './new-place.component.html',
  styleUrls: ['./new-place.component.css']
})
export class NewPlaceComponent implements AfterViewInit {
  public currentPlace: Place = new Place({});

  photoswipper: Object = {
    slidesPerView: 5,
    spaceBetween: 30,
    freeMode: true

  };

  public menuServerUrl: string;
  public categoryOrder = [];

  constructor(private repository: PlaceRepository,
              private placeService: PlaceService,
              private categoryRepository: CategoryRepository,
              private photoService: PlacePhotosService,
              private snackbar: MdSnackBar,
              private activeRoute: ActivatedRoute,
              private router: Router,
              private cropperDialog: MdDialog) {

    let id = this.activeRoute.snapshot.params['id'];

    if (id) {
      this.placeService.getPlaceById(id)
        .switchMap((param) => {
          this.currentPlace = param;
          return this.placeService.getOrderedCategoryList(this.currentPlace)
        }).subscribe((param) => {
        this.categoryOrder = param;

        this.categoryOrder = this.clearCategoryOrder(this.categoryOrder);


        if (this.currentPlace.menu) {
          this.repository.getMenuFileUrl(this.currentPlace).then(
            url => {
              this.menuServerUrl = url;
            }
          )
        }

        if (this.currentPlace.preview) {
          this.repository.getPreviewUrl(this.currentPlace).then(
            url => {
              this.currentPlace.previewFile.serverUrl = url || '';
            },
            err => console.error(err)
          );
        }

        this.currentPlace.photoList
          .filter((photo) => !!photo.id)
          .map((photo) => this.repository.getPhotoUrl(this.currentPlace, photo.id)
            .then(url => photo.serverUrl = url));

        this.currentPlace.extraPhotoList
          .filter((photo) => !!photo.id)
          .map((photo) => this.repository.getExtraPhotoUrl(this.currentPlace, photo.id)
            .then(url => photo.serverUrl = url))
      });
    }
  }

  private clearCategoryOrder(categoryOrder) {

    let onDelete = [];
    for (let cat_id in categoryOrder) {
      if (this.currentPlace.categoriesIdList.indexOf(cat_id) == -1) {
        onDelete.push(cat_id);
      }
    }

    for (let ondel of onDelete) {
      delete categoryOrder[ondel];
    }
    return categoryOrder
  }

  hasCategory(cat: Category) {
    return this.currentPlace.categoriesIdList.find(p => p == cat.id)
  }

  setAllCategoriesOrder(event) {
    if (event.target.checked == false) {
      for (let ord in this.categoryOrder) {
        this.categoryOrder[ord] = -1;
      }
    } else {
      for (let ord in this.categoryOrder) {
        if (this.categoryOrder[ord] == -1) {
          this.categoryOrder[ord] = 0;
        }
      }
    }

    console.log('Порядок категорий: ', this.categoryOrder)
  }

  setCategoryOrder(event, cat: Category) {
    this.categoryOrder[cat.id] = Number(event.target.value);

  }

  getCategoryOrder(cat: Category) {
    if (this.currentPlace.active) {
      if (!this.categoryOrder[cat.id]) {
        this.categoryOrder[cat.id] = 0;
      }
    }
    else
      this.categoryOrder[cat.id] = -1;

    return this.categoryOrder[cat.id];
  }

  addOrRemoveCategory(event, cat: Category) {
    if (event.target.checked) {
      if (!this.currentPlace.categoriesIdList.find(id => id == cat.id)) {
        this.currentPlace.categoriesIdList.push(cat.id);
      }
    } else {
      delete this.categoryOrder[cat.id];

      if (this.currentPlace.categoriesIdList.find(id => id == cat.id)) {

        this.currentPlace.categoriesIdList.splice(
          this.currentPlace.categoriesIdList.indexOf(
            this.currentPlace.categoriesIdList.find(id => id == cat.id)
          ), 1);
      }
    }
    console.log('categories', this.categoryOrder);
  }

  getCategories() {
    return this.categoryRepository.categories;
  }


  uploadPlace() {

    let activeCategoryOrder = JSON.parse(JSON.stringify(this.categoryOrder));
    activeCategoryOrder = this.clearCategoryOrder(activeCategoryOrder);

    if (this.currentPlace.getId()) {
      let id = this.currentPlace.getId();
      this.repository.updatePlace(this.currentPlace)
        .then(
          (snapshot) => {
            let bar = this.snackbar.open('Загрузка изображений', 'Закрыть');

            Promise.all([
              this.photoService.addPreview(
                this.currentPlace, this.currentPlace.previewFile),
              this.photoService.addPhotoListOnServer(
                this.currentPlace, this.currentPlace.photoList),
              this.photoService.addExtraPhotoListOnServer(
                this.currentPlace, this.currentPlace.extraPhotoList),
            ]).then((param) => {
              bar.dismiss();
              this.router.navigate(['places']);
            });
          })
        .then(
          snapshot2 => {

            this.placeService.addPlaceToCategoryPlaces(this.currentPlace, activeCategoryOrder);

          }
        );

    }
    else {
      this.repository.addPlace(this.currentPlace)
        .then(
          snapshot => {
            this.currentPlace = this.repository.getPlace(snapshot.key);
            this.placeService.addPlaceToCategoryPlaces(this.currentPlace, activeCategoryOrder);
            this.router.navigate(['places', 'edit', 'id', this.currentPlace.getId()]);
          }
        )

    }
  }

  uploadImage(width: number, height: number, storeImage, event) {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();

      reader.onload = (e) => {
        let config = new MdDialogConfig();
        config.data = {
          width: width,
          height: height,
          reader: e.target,
          file: event.target.files[0]
        };
        let dialogRef = this.cropperDialog.open(PhotoCropperComponent, config)
          .afterClosed().subscribe(photo => {
            if (photo != undefined) {
              storeImage(photo);
              event.target.value = null;
            }
          });

      };

      reader.readAsDataURL(event.target['files'][0]);
    }
  }

  public uploadPreviewPhoto = this.uploadImage
    .bind(this, 810, 360, (photo) => {
      this.currentPlace.previewFile = photo;
    });

  public uploadPhoto = this.uploadImage
    .bind(this, null, null, (photo) => {
      this.currentPlace.photoList.unshift(photo);
    });

  public uploadExtraPhoto = this.uploadImage
    .bind(this, null, null, (photo) => {
      this.currentPlace.extraPhotoList.unshift(photo);
    });


  public addMenuFile(event) {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();

      reader.onload = (e) => {
        this.repository.addMenuFile(this.currentPlace, event.target.files[0])
          .then(url => {
            this.repository.getMenuFileUrl(this.currentPlace).then(
              url => {
                this.menuServerUrl = url;
              }
            )
          });

      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  public removeMenuFile() {
    this.repository.removeMenuFile(this.currentPlace);
  }

  getPreviewUrl() {
    if (this.currentPlace.previewFile.serverUrl) {
      return this.currentPlace.previewFile.serverUrl;
    } else if (this.currentPlace.previewFile.file) {
      return this.currentPlace.previewFile.file.image;
    }
    return '';
  }

  getMenuFileUrl() {
    return this.menuServerUrl;
  }

  public removePhoto(id: number) {
    if (this.currentPlace.photoList[id].serverUrl) {
      this.repository.removePhoto(
        this.currentPlace,
        this.currentPlace.photoList[id].id)
    }
    this.currentPlace.photoList.splice(id, 1);

  }

  public removeExtraPhoto(id: number) {
    if (this.currentPlace.extraPhotoList[id].serverUrl) {
      this.repository.removeExtraPhoto(
        this.currentPlace,
        this.currentPlace.extraPhotoList[id].id)
    }
    this.currentPlace.extraPhotoList.splice(id, 1);

  }

  public removePreview() {
    if (this.currentPlace.previewFile.serverUrl) {
      this.repository.removePreview(
        this.currentPlace
      );

    }
    this.currentPlace.previewFile = new Photo();
  }

  // --- Getting photos --- //

  public getUrlList(photoList: Photo[]): string[] {
    return photoList.filter(p => p.file.image != '' && p.serverUrl == '')
      .map(p => p.file.image)
      .concat(
        photoList
          .filter(p => (p.serverUrl != ''))
          .map(p => p.serverUrl)
      )
  }

  /*
   photoList
   .filter(p => (p.serverUrl != ''))
   .map(p => p.serverUrl)
   .concat(
   */
  public getCurrentPlace(): Place {
    return this.currentPlace;
  }

  ngAfterViewInit() {

  }

}
