import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MdDialogModule, MdSnackBarModule, MdSelectModule} from '@angular/material';

import {AccessesDialogModule} from './accesses-dialog/accesses-dialog.module';
import {PhotoCropperModule} from './photo-cropper/photo-cropper.module';
import {ConfirmDialogModule} from './confirm-dialog/confirm-dialog.module';

import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabase} from 'angularfire2/database';
import {AngularFireAuth} from 'angularfire2/auth';
import {environment} from '../environments/environment';
import * as firebase from 'firebase';

import {SwiperModule} from 'angular2-useful-swiper';
import {ImageCropperModule} from 'ng2-img-cropper';

import {AppComponent} from './app.component';
import {PlacesTableComponent} from './places-table/places-table.component';
import {NewPlaceComponent} from './new-place/new-place.component';
import {route} from './app.routing';

import {PlaceRepository} from "./model/places.repository";
import {CategoryRepository} from "./model/category.repository";
import {PlacePhotosService} from "./shared/services/place-photos.service";
import {AuthService} from "./auth/auth.service";
import {DbRepository} from "./model/db.repository";

import {AuthGuard} from './auth/auth.guard';


import {KeysPipe} from './keys.pipe';
import {EditCategoryComponent} from './edit-category/edit-category.component';
import {CategoriesTableComponent} from './categories-table/categories-table.component';
import {LoginComponent} from './login/login.component';
import {CityListComponent} from './city-list/city-list.component';
import {CityListService} from "./shared/services/city-list.service";
import {PlaceService} from "./shared/services/place.service";

firebase.initializeApp(environment.firebase);

//firebase.auth().signInWithEmailAndPassword('admin@example.com', '12345678');

@NgModule({
  declarations: [
    AppComponent,
    PlacesTableComponent,
    KeysPipe,
    NewPlaceComponent,
    EditCategoryComponent,
    CategoriesTableComponent,
    LoginComponent,
    CityListComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    route,
    AngularFireModule.initializeApp(environment.firebase),
    SwiperModule,
    ImageCropperModule,

    BrowserAnimationsModule,
    MdDialogModule,
    MdSnackBarModule,
    MdSelectModule,

    AccessesDialogModule,
    PhotoCropperModule,
    ConfirmDialogModule
  ],
  providers: [PlaceRepository, CategoryRepository, DbRepository, PlaceService, CityListService,
    PlacePhotosService, AuthService, AuthGuard,
    AngularFireDatabase, AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule {

}
