import {Component, OnInit} from '@angular/core';
import {City} from './../model/city.model';
import {CityListService} from '../shared/services/city-list.service';

import {Observable} from 'rxjs';

import 'rxjs/add/operator/take';

@Component({
  selector: 'app-city-list',
  templateUrl: './city-list.component.html',
  styleUrls: ['./city-list.component.css']
})
export class CityListComponent implements OnInit {

  public cityList: City[] = [];
  public cityList$;

  public newCityName: string = '';

  constructor(private cityListService: CityListService) {

    this.cityList$ = cityListService.getCityObjects()
      .subscribe((response) => {
        this.cityList = response;
      });
  }

  public addCity(cityname: string) {
    this.cityListService.addCity(cityname);
    this.newCityName = '';
  }

  public updateCity(city: City) {
    this.cityListService.updateCityName(city);
    city.onEdit = false;
  }

  public removeCity(city: City) {
    this.cityListService.removeCity(city.id);
  }

  ngOnInit() {
  }

}
