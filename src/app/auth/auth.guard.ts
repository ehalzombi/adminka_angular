import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';

import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import {AuthService} from './auth.service';
import {Router, ActivatedRoute} from '@angular/router';


@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService,
              private router: Router) {
  }


  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {


    return new Observable<boolean>((observer) => {

      this.authService.authState$
        .subscribe((authState) => {

        if(!authState) {
          this.authService.userAllowed = false;
          this.router.navigate(['/login']);
          observer.next(false);
        }

          this.authService.getAdminObj()
            .subscribe((adminObj) => {
              if (authState && (adminObj == authState.uid)) {
                this.authService.userAllowed = true;
                observer.next(true);
              } else {
                this.authService.userAllowed = false;
                this.router.navigate(['/login']);
                observer.next(false);
              }
            })
        })
    })
    /*
     return this.authService.authState$
     .map(authState => {
     // @TODO Исправить проверку юзера на админство
     return !!authState && this.authService.checkUsrId(authState.uid);
     })
     .do(authed => {
     if (!authed) {
     this.router.navigate(['login']);
     }
     });
     */
  }
}
