import {Injectable, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AngularFireAuth} from 'angularfire2/auth';
import {AccessesDialogService} from './../accesses-dialog/accesses-dialog.service';
import {DbRepository} from './../model/db.repository';

import {Observable} from 'rxjs/Observable';

import * as firebase from 'firebase/app';
import {FirebaseError} from 'firebase/app';

@Injectable()
export class AuthService implements OnInit {
  public user: firebase.User;
  public authState$: Observable<firebase.User>;

  constructor(public afAuth: AngularFireAuth, private router: Router,
              private dbRep: DbRepository,
              public accesses: AccessesDialogService) {
    this.user = null;

    console.log('auth constructor');

    this.authState$ = afAuth.authState;
    this.dbRep.subscribeData('/admin')
      .subscribe(usr => {
        this.adminId = (<any>Object).values(usr)[0];
      });

    this.accesses.adminUserIdList()
      .subscribe(usrlist => {
        this.allowedUserList = usrlist;
      });

    this.authState$.subscribe((user: firebase.User) => {
      this.user = user;
    });
  }

  public allowedUserList = [];
  public userAllowed: boolean = false;
  private adminId = '';

  public checkUsrId(id: string): boolean {
    return id == this.adminId;
  }

  public getAdminObj() {
    return this.dbRep.subscribeData('/admin')
      .map((val, num) => (<any>Object).values(val)[0]);
  }

  get authenticated(): boolean {
    return this.user !== null;
  }

  get id(): string {
    return this.authenticated ? this.user.uid : null;
  }

  public loginUser(email: string, password: string) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password); //angularfire returns a promise
  }

  public logout() {
    return this.afAuth.auth.signOut();
  }

  public isAuthenticated(): Observable<any> {
    return this.afAuth.authState; //auth is already an observable
  }


  public permissedUser() {
    return this.checkUsrId(this.user.uid);
  }

  ngOnInit() {

  }
}
