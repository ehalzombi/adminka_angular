import {Component, OnInit} from '@angular/core';
import {MdDialog, MdDialogConfig} from '@angular/material';

import {Category} from '../model/category.model';
import {CategoryRepository} from './../model/category.repository';
import {ConfirmDialogComponent} from './../confirm-dialog/confirm-dialog.component';


@Component({
  selector: 'app-categories-table',
  templateUrl: './categories-table.component.html',
  styleUrls: ['./categories-table.component.css']
})
export class CategoriesTableComponent implements OnInit {



  constructor(private repository: CategoryRepository,
              private dialog: MdDialog) {
  }

  getCategoriesList() {
    return this.repository.categories
      .filter((cat) => cat.id != '$value');
  }

  removeCategory(cat: Category) {
    let config = new MdDialogConfig();
    config.data = {
      dialogText: 'Вы действительно хотите удалить '+cat.name+'?',
      activeButtonClass: 'btn-danger',
      activeButtonText: 'Да'
    };

    let dialogRef = this.dialog.open(ConfirmDialogComponent, config);
    dialogRef.afterClosed()
      .subscribe(snapshot => {
        if(snapshot) {
          this.repository.removeCategory(cat.id)
            .then(snapshot => {
              this.repository.removeFromCategoryList(cat.id);
            });
        }

      });
  }

  deannounceCategory(cat: Category) {
    cat.announced = false;
    this.repository.updateCategory(cat);
  }

  announceCategory(cat: Category) {
    cat.announced = true;
    this.repository.updateCategory(cat);
  }

  ngOnInit() {

  }

}
