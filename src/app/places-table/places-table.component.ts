import {Component, OnInit} from '@angular/core';
import {MdDialog, MdDialogConfig} from '@angular/material';

import {Place} from '../model/place.model';
import {PlaceRepository} from './../model/places.repository';
import {AccessesDialogComponent} from './../accesses-dialog/accesses-dialog.component';
import {ConfirmDialogComponent} from './../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-places-table',
  templateUrl: './places-table.component.html',
  styleUrls: ['./places-table.component.css']
})
export class PlacesTableComponent implements OnInit {

  constructor(private repository: PlaceRepository,
              private dialog: MdDialog) {
    this.repository.getPlacesList()
      .subscribe((pl) => {
        this.placesList = pl.map((obj) => <Place>obj);
      });
  }

  openDialog(place: Place) {
    let config = new MdDialogConfig();
    config.data = place;

    let dialogRef = this.dialog.open(AccessesDialogComponent, config);
    dialogRef.afterClosed()
      .subscribe(
        snapshot => console.log('closed')
      );
  }

  private placesList: Place[];

  getPlacesList(): Place[] {
    return this.placesList;
  }

  removePlace(place: Place) {
    let config = new MdDialogConfig();
    config.data = {
      dialogText: 'Вы действительно хотите удалить ' + place.name + '?',
      activeButtonClass: 'btn-danger',
      activeButtonText: 'Да'
    };

    let dialogRef = this.dialog.open(ConfirmDialogComponent, config);
    dialogRef.afterClosed()
      .subscribe(
        (param) => {
          if(param)
            this.repository.removePlace(place);
        }
      );

  }

  togglePlace(place: Place) {
    if (place.active) {
      place.active = false;
    } else if (!place.active) {
      place.active = true;
    }
    this.repository.updatePlace(place);
  }

  ngOnInit() {
  }

}
