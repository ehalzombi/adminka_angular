import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {AuthService} from '../auth/auth.service';
import {Router} from '@angular/router';


@Component({
  moduleId: module.id,
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private sub: Subscription;
  public email: string = '';
  public password: string = '';

  constructor(private authService: AuthService,
              private router: Router) {
  }


  authUser() {
    this.authService.loginUser(this.email, this.password)
      .then(snapshot => {
        this.incorrectUser = false;
        this.router.navigate(['/places']);
        if(!this.authService.userAllowed) {
          this.permissedUser = false;
        } else {
          this.permissedUser = true;
        }
      })
      .catch((err) => {
        this.permissedUser = true;
        this.incorrectUser = true;
      })
  }

  public incorrectUser: boolean = false;
  public permissedUser: boolean = true;

  ngOnInit() {
    this.sub = this.authService.isAuthenticated().subscribe();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
