export const environment = {
  production: true,

  firebase: {
    apiKey: 'AIzaSyCUBMnU2RrAURbNGZN21vbR_9n9Box99Sk',
    authDomain: 'clicknbook-adc17.firebaseapp.com',
    databaseURL: "https://clicknbook-adc17.firebaseio.com",
    storageBucket: 'clicknbook-adc17.appspot.com',
  }
};
