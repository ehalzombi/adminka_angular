// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

  firebase: {
    apiKey: 'AIzaSyDP9QLEpjn8RGpvoNtzHyeSO-D5r-7GPcM',
    authDomain: 'restticket-c5ba1.firebaseapp.com',
    databaseURL: "https://restticket-c5ba1.firebaseio.com",
    storageBucket: 'restticket-c5ba1.appspot.com',
  }
};
