import { AdminkaPage } from './app.po';

describe('adminka App', () => {
  let page: AdminkaPage;

  beforeEach(() => {
    page = new AdminkaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
